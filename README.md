# README #

Core package for mrfert, the planner is here. It uses OMPL for data structures, and implements a modified version of a random sampling optimal planner such as RRT*.

You will need the latest OMPL version compiled from source code (ROS or Ubuntu do not provide the latest one) as described here:

[ompl ros install](https://github.com/ompl/ompl/blob/master/doc/markdown/installation.md#installation-from-source-for-use-with-ros)

For convenience, here is a copy of those instructions:

### Installation of OMPL from source for use with ROS

If you'd like to use your own build of OMPL with ROS, follow the following steps:

- [Create a normal ROS catkin workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)
- [Clone OMPL 1.1.0 into the src/ folder using the ZIP file here](http://ompl.kavrakilab.org/download.html)
- Add the package.xml file:
```
wget https://raw.githubusercontent.com/ros-gbp/ompl-release/debian/`rosversion -d`/`lsb_release -cs`/ompl/package.xml
```
- To build, you must run catkin_make_isolated instead of the normal catkin_make since OMPL is not a normal catkin package.
- When sourcing this workspace, be sure to source devel_isolated/setup.bash.




Using the planner directly is possible in a centralized approach, but usually multiple launch files are used for distributed tests with multiple robots, see mrfert_maps for some examples.

Alternatively, in order to run the main.cpp example in the src folder, try
```
rosrun mrfert mrfert_node
```

and see what parameters are required.


## Troubleshooting

- You may need to run catktin_make_isolated multiple times in order to compile OMPL in the ROS workspace
- Tf errors may appear when launching gazebo, they should stop after a couple of seconds
- Remember to source both devel_isolated/setup.bash and devel/setup.bash