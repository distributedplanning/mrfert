#include <ros/package.h>
#include <ros/init.h>
#include <ros/node_handle.h>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <gmlreader/graphmsgs_wrapper.hpp>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <mrfert/Plane2DEnvironment.h>


int main(int argc, char ** argv)
{
    //Hello
    ros::init(argc,argv,"map_node");
    std::string path=ros::package::getPath("mrfert");
    ros::NodeHandle nh("~");

    double map_scale;
    double graph_scale;
    std::string filename;
    std::string temp_name;
    if (nh.searchParam("map_scale",temp_name))
    {
	nh.getParam(temp_name,map_scale);
	std::cout<<"--- map_node --- map_scale parameter, using "<<map_scale<<std::endl;
    }
    else
    {
        map_scale = 0.05;
        std::cout<<"please specify a map_scale parameter, using default as "<<map_scale<<std::endl;
    }
        
    if (nh.searchParam("graph_scale",temp_name))
    {
        nh.getParam(temp_name,graph_scale);
	std::cout<<"--- map_node --- graph_scale parameter, using "<<graph_scale<<std::endl;
    }
    else
    {
        graph_scale = 0.01;
        std::cout<<"please specify a graph_scale parameter, using default as "<<graph_scale<<std::endl;
    }
    
    if (nh.searchParam("filename",temp_name))
    {
        nh.getParam(temp_name,filename);
	std::cout<<"--- map_node --- filename parameter, using "<<filename<<std::endl;
    }
    else
    {
        filename="Test4";
        std::cout<<"please specify a filename parameter for *.gml and *.ppm, using default as "<<filename<<std::endl;
    }
        
    //Load graph nodes
    graph_msgs::GeometryGraph graph;
    std::ifstream fs(/*path+"/"+*/filename+".gml");
    if (!fs.is_open())
    {
        std::cout<<"--- Publish_map --- could not open "<< /*path+"/"+*/filename+".gml"<<std::endl;
        abort();
    }
    graphmsgs_gml reader;
    reader.read(fs,graph,graph_scale);

    //Make arcs uniques (just in case voronoi created multiple arcs between the same nodes)
    for (int i=0;i<graph.edges.size();i++)
    {
        std::sort(graph.edges[i].node_ids.begin(),graph.edges[i].node_ids.end());
        graph.edges[i].node_ids.erase(std::unique(graph.edges[i].node_ids.begin(),graph.edges[i].node_ids.end()),graph.edges[i].node_ids.end());
    }
    
    //Load xy for each node, just to check that start and goal node exist
    std::map<int,std::map<int,int>> xy_index;
    int i=0;
    for (auto& node:graph.nodes)
    {
        //Change of reference frame because voronoi works with screen coordinates, while gml uses robot coordinates
        node.y= node.y;

        xy_index[(int)round(node.x)][(int)round(node.y)]=i;
        i++;
    }
    
    ros::NodeHandle n;
    rviz_visual_tools::RvizVisualTools r("map","/test");
    ros::AsyncSpinner s(1);
    s.start();
    for (auto& n:graph.nodes)
    {
	n.x*=map_scale/graph_scale;
	n.y*=map_scale/graph_scale;
        n.z=0;
    }
    while(ros::ok())
    {
        r.publishGraph(graph,rviz_visual_tools::BLUE,0.05);
        geometry_msgs::Pose pose1;
        r.publishAxis(pose1);
        ros::Duration(0.5).sleep();
        ros::spinOnce();
    }
    return 0;
}
