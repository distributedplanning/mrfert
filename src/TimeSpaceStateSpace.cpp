#include "mrfert/TimeSpaceStateSpace.h"
#include "ompl/tools/config/MagicConstants.h"
#include "mrfert/TimeSpaceLengthOptimizationObjective.h"
#include <cstring>

typedef ompl::base::XYTStateSpace::StateType XYTState;
using namespace std;

ompl::base::State* ompl::base::XYTStateSpace::allocState() const
{
    StateType *state = new StateType();
    allocStateComponents(state);
    return state;
}

void ompl::base::XYTStateSpace::freeState(ompl::base::State* state) const
{
    CompoundStateSpace::freeState(state);
}

ompl::base::StateSamplerPtr ompl::base::XYTStateSpace::allocStateSampler() const
{
    return ompl::base::StateSpace::allocStateSampler();
}

double ompl::base::XYTStateSpace::distance(const ompl::base::State* s1, const ompl::base::State* s2) const
{
    const int w1 = s1->as<XYTState>()->getX();
    const int h1 = s1->as<XYTState>()->getY();
    const double t1 = s1->as<XYTState>()->getT();
    const int w2 = s2->as<XYTState>()->getX();
    const int h2 = s2->as<XYTState>()->getY();
    const double t2 = s2->as<XYTState>()->getT();
    double deltaT=t2-t1;
    double d=sqrt(pow(w1-w2,2)+pow(h1-h2,2));
    return d+1*deltaT;
}


void ompl::base::XYTStateSpace::registerProjections()
{
    class XYTDefaultProjection : public ProjectionEvaluator
    {
    public:
        
        XYTDefaultProjection(const StateSpace *space) : ProjectionEvaluator(space)
        {
        }
        
        virtual unsigned int getDimension() const
        {
            return 3;
        }
        
        virtual void defaultCellSizes()
        {
            cellSizes_.resize(3);
            bounds_ = space_->as<XYTStateSpace>()->getBounds();
            cellSizes_[0] = (bounds_.high[0] - bounds_.low[0]) / magic::PROJECTION_DIMENSION_SPLITS;
            cellSizes_[1] = (bounds_.high[1] - bounds_.low[1]) / magic::PROJECTION_DIMENSION_SPLITS;
            cellSizes_[2] = (bounds_.high[2] - bounds_.low[2]) / magic::PROJECTION_DIMENSION_SPLITS;
        }
        
        virtual void project(const State *state, EuclideanProjection &projection) const
        {
            memcpy(&projection(0), state->as<XYTStateSpace::StateType>()->as<RealVectorStateSpace::StateType>(0)->values, 2 * sizeof(double));
        }
    };
    
    registerDefaultProjection(ProjectionEvaluatorPtr(dynamic_cast<ProjectionEvaluator*>(new XYTDefaultProjection(this))));
}
