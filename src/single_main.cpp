#include "ros/ros.h"
#include <ros/package.h>
#include <geometry_msgs/Polygon.h>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <tf/transform_listener.h>
#include <tf/tf.h>
#include <sstream>
#include <math.h>
#include <mrfert/Plane2DEnvironment.h>
#include <mrfert/locking.h>
#include <gmlreader/graphmsgs_wrapper.hpp>
#include <mrfert/single_agent_planner.h>

using namespace std;
using namespace ompl::geometric;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "single_planner");
    
    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;
//     ompl::msg::setLogLevel(ompl::msg::LOG_NONE);
    std::string path=ros::package::getPath("mrfert");
    ros::NodeHandle nh_p("~");
    ros::NodeHandle nh;
    //Load ros parameters
    std::string filename;
    int index_start,index_end;
    double graph_scale;
    int priority;
    std::string temp_name;
    if (nh.searchParam("filename",temp_name))
        nh.getParam(temp_name,filename);
    else
    {
        filename="Test4";
        ROS_WARN_STREAM("please specify a filename parameter for *.gml and *.ppm, using default as "<<filename);
    }
    if (!nh_p.getParam("priority",priority))
    {
        ROS_FATAL_STREAM("please specify a priority");
        return 0;
    }
    
    if (!nh_p.getParam("start",index_start))
    {
        ROS_FATAL_STREAM("please specify a start");
        return 0;
    }
    
    if (!nh_p.getParam("goal",index_end))
    {
        ROS_FATAL_STREAM("please specify a goal");
        return 0;
    }
    
    if (nh.searchParam("graph_scale",temp_name))
    {
        nh.getParam(temp_name,graph_scale);
	//std::cout<<" --- SingleAgent ---graph_scale parameter, using "<<graph_scale<<std::endl;
    }
    else
    {
        graph_scale = 0.01;
        ROS_WARN_STREAM("please specify a graph_scale parameter, using default as "<<graph_scale);
    }
    
    double map_scale;
    if (nh.searchParam("map_scale",temp_name))
    {
        nh.getParam(temp_name,map_scale);
	//std::cout<<" --- SingleAgent ---map_scale parameter, using "<<map_scale<<std::endl;
    }
    else
    {
        std::cout<<"please specify a map_scale parameter, using default as "<<0.2<<std::endl;
        map_scale = 0.2;
    }
    
    std::string prefix,agent_name;
    int left=0;
    if (nh.getNamespace().find("single_planner")!=std::string::npos)
        left = std::string("single_planner").length();
    prefix = nh.getNamespace().substr(0,nh_p.getNamespace().length()-left);
    prefix=tf::strip_leading_slash(prefix);
    ROS_INFO_STREAM("prefix "<<prefix);
    if(!nh_p.getParam("private_name", agent_name))
    {
        if (agent_name=="")
        {
            if (prefix!="") agent_name=prefix;
            else
            {
                agent_name="iRobot";
                ROS_WARN("missing agent name AND namespace, using default values");
            }
        }
        else prefix=agent_name;
    }
    else
        prefix=agent_name;
    ros::Rate loop_rate(15);
    //Load graph nodes
    graph_msgs::GeometryGraph graph;
    std::ifstream fs(/*path+"/"+*/filename+".gml");
    if (!fs.is_open()){
        ROS_FATAL_STREAM(agent_name << " could not open "<</*path+"/"+*/filename+".gml");
    }
    graphmsgs_gml reader;
    reader.read(fs,graph,graph_scale);
    single_agent_planner planner(graph,agent_name,priority,index_start,index_end);
    
    tf::TransformListener listener;
    ros::Publisher speed_control = nh.advertise<mrfert::locking>("targets",1,true);
    mrfert::locking new_targets;
    ros::AsyncSpinner spinner(1);
    spinner.start();
    planner.init();
    //Load the environment, with the ppm, graph and other parameters
    bool localized=false;
    while(ros::ok())
    {
        tf::StampedTransform transform;
        try{
            listener.lookupTransform("/map",prefix+"/base_link", 
                                     ros::Time(0), transform);
            if (!localized)
                ROS_INFO_STREAM( agent_name <<" current (Map srs) on "<< transform.getOrigin().getX() << " " << transform.getOrigin().getY() );
            localized=true;
        }
        catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
        }
        
        // the planner works in graph_scale srs
        planner.setPosition(transform.getOrigin().getX(),transform.getOrigin().getY());
        planner.run();
	
        std::vector<mrfert::node> sol;
	// get solution in map_scale srs
        if (planner.updated_solution(sol))
        {
            new_targets.command="switch";
            new_targets.occupied_nodes=sol;
	        
	    // the speed control works in map_scale srs
            speed_control.publish(new_targets);
            planner.resetUpdate();
        }
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
