#include "mrfert/Plane2DMotionValidator.h"
#include <algorithm>

int Plane2DMotionValidator::skipped = 0;
int Plane2DMotionValidator::too_fast = 0;
int Plane2DMotionValidator::checked = 0;

Plane2DMotionValidator::Plane2DMotionValidator(const ompl::base::SpaceInformationPtr& si, const graph_msgs::GeometryGraph& graph, double max_speed)
:DiscreteMotionValidator(si),max_speed(max_speed),graph(graph)
{
/*    int i=0;
    for (auto node:graph.nodes)
    {
        xy_index[(int)round(node.x)][(int)round(node.y)]=i;
        i++;
    }*/
}

bool Plane2DMotionValidator::checkMotion(const ompl::base::State* s1, const ompl::base::State* s2) const
{
    const int w1 = (int)round(s1->as<XYTState>()->getX());
    const int h1 = (int)round(s1->as<XYTState>()->getY());
    const int w2 = (int)round(s2->as<XYTState>()->getX());
    const int h2 = (int)round(s2->as<XYTState>()->getY());
    this->checked++;
    //Note that the commented code below is useless if the sampler is doing its job
/*  if (xy_index.count(w1) && xy_index.count(w2) && xy_index.at(w1).count(h1) && xy_index.at(w2).count(h2))
    {
 //     std::cout<<"checking "<<xy_index.at(w1).at(h1)<<" "<<xy_index.at(w2).at(h2)<<std::endl;
        if (std::find(graph.edges[xy_index.at(w1).at(h1)].node_ids.begin(),graph.edges[xy_index.at(w1).at(h1)].node_ids.end(),xy_index.at(w2).at(h2))!=graph.edges[xy_index.at(w1).at(h1)].node_ids.end()) //The graph already guarantees collision free motions
        {
        //nothing to do, just avoid next check
 //         std::cout<<"skipped a state check"<<std::endl;
            skipped++;
        }
        else return false;
    }
    else abort();*/

    //Check for the speed limit
    const double t1 = (double)s1->as<XYTState>()->getT();
    const double t2 = (double)s2->as<XYTState>()->getT();
    double deltaT=t2-t1;
    if (deltaT<=0)
    {
        too_fast++;
        return false;
    }
    double d=sqrt(pow(w1-w2,2)+pow(h1-h2,2));
    if ((d/deltaT)<max_speed)
    {
//         std::cout<<"C "<<w1<<" "<<h1<<" "<<t1<<" "<<w2<<" "<<h2<<" "<<t2;
//         std::cout<<" ("<<d/deltaT<<" < "<<max_speed<<")"<<std::endl;
        return true;
    }
    else return false;
}


bool Plane2DMotionValidator::checkMotion(const ompl::base::State* s1, const ompl::base::State* s2, std::pair< ompl::base::State*, double >& lastValid) const
{
    throw "not implemented";
    //Actually the following implementation is working, but I don't want it to be called by anyone because it is slow!!
    /*    const int w1 = (int)round(s1->as<XYTState>()->getX());
     *    const int h1 = (int)round(s1->as<XYTState>()->getY());
     *    const int w2 = (int)round(s2->as<XYTState>()->getX());
     *    const int h2 = (int)round(s2->as<XYTState>()->getY());
     *    std::cout<<"high check motion"<<std::endl;
     *    if (xy_index.count(w1) && xy_index.count(w2) && xy_index.at(w1).count(h1) && xy_index.at(w2).count(h2))
     *        if (std::find(graph.edges[xy_index.at(w1).at(h1)].node_ids.begin(),graph.edges[xy_index.at(w1).at(h1)].node_ids.end(),xy_index.at(w2).at(h2))!=graph.edges[xy_index.at(w1).at(h1)].node_ids.end()) //The graph already guarantees collision free motions
     *        {
     *            //nothing to do, just avoid next checks
}
else return false; 
//     else if (!DiscreteMotionValidator::checkMotion(s1,s2,lastValid)) return false;
if (checkMotion(s1,s2))
    return true;
else//This is just a test, I should find where the maximum speed is no longer respected, but not now
{
const int w1 = (int)s1->as<XYTState>()->getX();
const int h1 = (int)s1->as<XYTState>()->getY();
const int t1 = (int)s1->as<XYTState>()->getT();
ob::State *t = si_->allocState();
t->as<XYTState>()->get(0)=w1;
t->as<XYTState>()->get(1)=h1;
t->as<XYTState>()->get(2)=t1;
lastValid.first=t;
lastValid.second=0;
}*/
}
