#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

std::string 
myfunc3(void)
{
    int j, nptrs;
    #define SIZE 100
    void *buffer[100];
    char **strings;
    
    nptrs = backtrace(buffer, SIZE);
    //     printf("backtrace() returned %d addresses\n", nptrs);
    
    /* The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)
     *       would produce similar output to the following: */
    
    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL) {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }
    std::string back;
    for (j = 0; j < nptrs; j++)
    {
        //printf("%s\n", strings[j]);
        back.append(strings[j]);
        back.append("\n");
    }
    
    free(strings);
    return back;
}

std::map<ompl::geometric::MRTstar::Motion*, std::string> ompl::geometric::MRTstar::tracem;


bool ompl::geometric::MRTstar::checkAdd(boost::shared_ptr< NearestNeighbors<Motion*> >& nn_, Motion* motion )
{
    static int counter=0;
    counter++;
    std::vector<Motion* > data;
    nn_->list(data);
    std::sort(data.begin(),data.end());
    Motion * temp;
    bool first=true;
    for (auto child:data)
    {
        if (child==motion)
        {
            std::cout<<"trying to add a motion which is already there "<<myfunc3()<<std::endl;
            std::cout<<"and was added during "<<tracem.at(motion)<<std::endl;
        }
        
        if (first)
        {
            temp=child;
            first=false;
        }
        else
        {
            if (temp==child) 
            {
                std::cout<<counter<<" the tree has two equal motions "<<temp<< " "<<myfunc3()<<std::endl;
                abort();
            }
        }
        temp=child;
    }
    tracem[motion]=myfunc3();
}


bool ompl::geometric::MRTstar::checkAddM(boost::shared_ptr< NearestNeighbors<Motion*> >& nn_, Motion* motion )
{
    return checkAdd(nn_,motion);
}

bool ompl::geometric::MRTstar::checkAddO(boost::shared_ptr< NearestNeighbors<Motion*> >& nn_, Motion* motion )
{
    return checkAdd(nn_,motion);
    
}

bool ompl::geometric::MRTstar::checkAddN(boost::shared_ptr< NearestNeighbors<Motion*> >& nn_, Motion* motion )
{
    return checkAdd(nn_,motion);
    
}

void destroy()
{
    static int errors=0;                    
    static std::map<void*,std::string> tracemap;
    auto t = std::chrono::high_resolution_clock::now();
    if(deleted) 
    {
        std::cout<<"called destroy of a motion already flagged as deleted "<<this<<" "<<errors++<<std::endl;
        std::cout<<"first destroy was called "<<tracemap.at(this)<<std::endl;
        std::cout<<"--------"<<std::endl;
        std::cout<<"second destroy was called "<<myfunc3()<<" "<<t.time_since_epoch().count()/1000%10000000000<<std::endl;
    }
    tracemap[this]=myfunc3();
    tracemap[this].append(std::to_string(t.time_since_epoch().count()/1000%10000000000));
    deleted=true;
}



