#include "mrfert/Plane2DGoalRegion.h"
#include "mrfert/TimeSpaceStateSpace.h"

namespace ob = ompl::base;
typedef ob::XYTStateSpace::StateType XYTState;

using namespace ob;


Plane2DGoalRegion::Plane2DGoalRegion(const ompl::base::SpaceInformationPtr& si,int x, int y):GoalRegion(si),x(x),y(y)
{
    this->si_=si;
    this->threshold_=0.01;
}

double Plane2DGoalRegion::distanceGoal(const ompl::base::State* state) const
{
    const int w = (int)state->as<XYTState>()->getX();
    const int h = (int)state->as<XYTState>()->getY();
    
    if (sqrt(pow(x-w,2)+pow(y-h,2))<threshold_) 
      std::cout<<"D found a final state (Graph srs) "<<w<<" "<<h<<std::endl;
    
    return sqrt(pow(x-w,2)+pow(y-h,2));
}


bool Plane2DGoalRegion::isSatisfied(const State* state, double* distance) const
{
    const int w = (int)state->as<XYTState>()->getX();
    const int h = (int)state->as<XYTState>()->getY();
    *distance = sqrt(pow(x-w,2)+pow(y-h,2));
    if (*distance<threshold_) 
      return true;
    
    return false;
}
