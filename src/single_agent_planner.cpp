#include "ros/ros.h"
#include <ros/package.h>
#include <geometry_msgs/Polygon.h>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <tf/transform_listener.h>
#include <sstream>
#include <math.h>
#include <mrfert/Plane2DEnvironment.h>
#include <mrfert/locking.h>
#include <gmlreader/graphmsgs_wrapper.hpp>
#include <mrfert/single_agent_planner.h>

using namespace std;
using namespace ompl::geometric;

#define ARC_MAX_LENGTH 6

const double z_magnification = 1./20.;

//////////////////////////////////////////////////////////////////////////
geometry_msgs::Point convert2DPoint_single(mrfert::node & node_, ros::Time zero)
{
  ros::Time now = zero;
  geometry_msgs::Point point;

  point.x=node_.x;
  point.y=node_.y;
  
  //point.z=z_magnification*((node_.t_start)+(node_.t_end-node_.t_start)*0.5).toSec();
  //point.z=(int(point.z)%1000)*0.1;
  
  point.z= z_magnification * ((node_.t_start-now) + (node_.t_end-now)).toSec() / 2.0 ;
  //point.z=int(point.z)%1000;
  //std::cout<<point.x<<" "<<point.y<<" "<<point.z<<std::endl;
  return point;
}

//////////////////////////////////////////////////////////////////////////
std::vector<geometry_msgs::Point> convert2DSolution_single(std::vector<mrfert::node>& sol, ros::Time zero)
{
    std::vector<geometry_msgs::Point> res;
    
    for (auto p:sol)
      res.push_back(convert2DPoint_single(p,zero));
    return res;
}

//////////////////////////////////////////////////////////////////////////
graph_msgs::GeometryGraph convert2Graph_single(std::vector<mrfert::node>& sol_, ros::Time zero)
{
  graph_msgs::GeometryGraph l_graph;
  
  std::vector<geometry_msgs::Point> l_points = convert2DSolution_single(sol_, zero);
  l_graph.nodes = l_points;
  for(size_t i=0; i < l_graph.nodes.size(); ++i)
  {
    graph_msgs::Edges edgeContainer;
    if(i != l_graph.nodes.size()-1)
      edgeContainer.node_ids.push_back(i+1);
    l_graph.edges.push_back(edgeContainer);
  }
    
  return l_graph;
}

//////////////////////////////////////////////////////////////////////////
void publishSolution_single(
  rviz_visual_tools::RvizVisualTools& rviz,
  std::vector<mrfert::node>& sol_, 
  rviz_visual_tools::colors color_,
  std::string name_,
  double const& map_scale_,
  ros::Time zero)
{
  std::cout << name_ << " publish solution" << std::endl;
  
  std::vector<geometry_msgs::Point> l_points = convert2DSolution_single(sol_, zero);
  rviz.publishPath(l_points, color_, rviz_visual_tools::REGULAR, name_+"arcs");
  rviz.publishGraph(convert2Graph_single(sol_, zero), color_, 0.01); 
    
  auto now=zero;
  int count = 0;
  rviz_visual_tools::colors l_sphereColor = color_;
  for(auto l_node:sol_)
  {
    geometry_msgs::Point l_point = convert2DPoint_single(l_node, zero);

    geometry_msgs::Pose l_pose;
    l_pose.position=l_point;
    geometry_msgs::Vector3 l_scale; 

    double l_h = z_magnification * ((l_node.t_end-l_node.t_start)*0.5).toSec() ;
        
    if( (count == 0) || (count == sol_.size()-1) )
      rviz.publishSphere(l_pose, l_sphereColor, 0.05, name_+"nodes");
    else
      rviz.publishCylinder(l_pose, l_sphereColor, l_h, 0.05, name_+"nodes"); 
    
    count++;
  }
  
  geometry_msgs::Pose pose1;
  rviz.publishAxis(pose1);
  ros::Duration(0.5).sleep();
}

//////////////////////////////////////////////////////////////////////////
my_rviz_visual_tools::my_rviz_visual_tools(string base_frame_, string marker_topic_):RvizVisualTools(base_frame_,marker_topic_)
{}

//////////////////////////////////////////////////////////////////////////
bool my_rviz_visual_tools::publishPath(const vector< geometry_msgs::Point >& path, const rviz_visual_tools::colors& color, const rviz_visual_tools::scales& scale, const string& ns, int id)
{
    if (path.size() < 2)
    {
        ROS_WARN_STREAM_NAMED(name_, "Skipping path because " << path.size() << " points passed in.");
        return true;
    }
    
    line_list_marker_.header.stamp = ros::Time();
    line_list_marker_.ns = ns;
    
    if (id==0)
    // Provide a new id every call to this function
        line_list_marker_.id++;
    else
        line_list_marker_.id=id;
    std_msgs::ColorRGBA this_color = getColor(color);
    line_list_marker_.scale.x = 1.;//getScale(scale, false, 1.5);
    line_list_marker_.scale.y = 1.;
    line_list_marker_.scale.z = 1.;
    line_list_marker_.color = this_color;
    line_list_marker_.points.clear();
    line_list_marker_.colors.clear();
    
    // Convert path coordinates
    for (std::size_t i = 1; i < path.size(); ++i)
    {
        // Add the point pair to the line message
        line_list_marker_.points.push_back(path[i - 1]);
        line_list_marker_.points.push_back(path[i]);
        line_list_marker_.colors.push_back(this_color);
        line_list_marker_.colors.push_back(this_color);
    }
    
    // Helper for publishing rviz markers
    return publishMarker(line_list_marker_);
}

//////////////////////////////////////////////////////////////////////////
single_agent_planner::single_agent_planner(graph_msgs::GeometryGraph graph, string name, int priority, int start, int end):
agent_name(name),priority(priority),graph(graph),rviz("map","/test")
{
    //Load xy for each node, just to check that start and goal node exist
    std::map<int,std::map<int,int>> xy_index;
    int i=0;
    for (auto& node:graph.nodes)
    {
        node.x=node.x;
        node.y=node.y;
//         std::cout<<node.x<<" "<<node.y<<std::endl;
        xy_index[int(node.x)][int(node.y)]=i;
        i++;
    }
    std::cout<<agent_name<<" starting from (Graph srs 1) "<< graph.nodes.at(start).x<<" "<<graph.nodes.at(start).y<<" to "<<graph.nodes.at(end).x<<" "<<graph.nodes.at(end).y<<std::endl;    
    global_x=graph.nodes.at(end).x;
    global_y=graph.nodes.at(end).y;
    start_x=graph.nodes.at(start).x;
    start_y=graph.nodes.at(start).y;
    xy_index.at(start_x).at(start_y);
    xy_index.at(global_x).at(global_y);
    std::cout<<agent_name<<" starting from (Graph srs 2) "<<start_x<<" "<<start_y<<" to "<<global_x<<" "<<global_y<<std::endl;
    
    agents = nh.subscribe<mrfert::locking>("/occupied",50, &single_agent_planner::agent_callback,this);
    locking_pub = nh.advertise<mrfert::locking>("/occupied",1);
    keep_alive = nh.advertise<mrfert::locking>("/alive",1);
    sub_alive = nh.subscribe<mrfert::locking>("/alive",50,&single_agent_planner::alive_callback,this);
    alive.priority=priority;
    ros::NodeHandle nh_p("~");
    std::string temp_name;
    if (nh.searchParam("max_speed",temp_name))
        nh.getParam(temp_name,speed);
    else
    {
        std::cout<<"please specify a max_speed parameter, using default as "<<2.0<<std::endl;
        speed = 2.0;
    }
    
    if (nh.searchParam("max_time",temp_name))
    {
	nh.getParam(temp_name,max_time);
	//std::cout<<" --- SingleAgent --- " << agent_name << " max_time parameter, using "<<max_time<<std::endl;
    }
    else
    {
        std::cout<<"please specify a max_time parameter, using default as "<<500<<std::endl;
        max_time = 500;
    }
    if (nh.searchParam("map_scale",temp_name))
    {
        nh.getParam(temp_name,map_scale);
	//std::cout<<" --- SingleAgent --- " << agent_name << " map_scale parameter, using "<<map_scale<<std::endl;
    }
    else
    {
        std::cout<<"please specify a map_scale parameter, using default as "<<0.2<<std::endl;
        map_scale = 0.2;
    }
    
    if (nh.searchParam("graph_scale",temp_name))
    {
        nh.getParam(temp_name,graph_scale);
	//std::cout<<" --- SingleAgent --- " << agent_name << " graph_scale parameter, using "<<graph_scale<<std::endl;
    }
    else
    {
        graph_scale = 0.01;
        ROS_WARN_STREAM("please specify a graph_scale parameter, using default as "<<graph_scale);
    }
    
    double occupied_node_time;
    if ( nh.searchParam("occupied_node_time",temp_name) )
    {
        nh.getParam(temp_name,occupied_node_time);
	//std::cout<<" --- SingleAgent --- " << agent_name << " occupied_time parameter, using "<<occupied_node_time<<std::endl;
    }
    else
    {
        occupied_node_time = 5;
        ROS_WARN_STREAM("please specify a occupied_node_time parameter, using default as "<<occupied_node_time);
    }
    
    env=std::make_shared<Plane2DEnvironment>(
      graph, 
      speed/map_scale*graph_scale, 
      max_time,
      occupied_node_time);
    
    collision=false;
    collision_mutex.unlock();
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::send_locking_info(const mrfert::locking& msg)
{
    locking_pub.publish(msg);
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::alive_callback(const mrfert::locking::ConstPtr& msg)
{
    if (msg->priority != priority  &&
	dist(msg->x,msg->y,x,y) < (2.0*ARC_MAX_LENGTH / map_scale * graph_scale) )
    {
	//ROS_INFO_STREAM(msg->x<<" "<<msg->y<<" "<<x<<" "<<y<<" "<<dist(msg->x,msg->y,x,y));
        std::unique_lock<std::mutex> lock(collision_mutex);
        mrfert::locking lck;
        env->getSolution(lck.occupied_nodes,last_updated);
        lck.priority=priority;
        lck.x=x;
        lck.y=y;
        send_locking_info(lck);
    }
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::resetUpdate()
{
    updated=false;
}

//////////////////////////////////////////////////////////////////////////
bool single_agent_planner::updated_solution(vector< mrfert::node >& sol)
{ 
    if (updated)
      // publish message to control
    {
      // add map_scale on output
	sol=lck.occupied_nodes;
	
	for(auto & node:sol)
	{ 
	  node.x *= map_scale / graph_scale;
	  node.y *= map_scale / graph_scale;
	  
	  //std::cout<<agent_name << " Trajectory (Map srs) "<< node.x<<" " <<node.y<<std::endl;
	}
	
	std::cout << agent_name << " update solution called" <<std::endl; 
	
	auto color=rviz_visual_tools::BLACK;
        switch (priority){
            case 1:
                color=rviz_visual_tools::RED;
                break;
            case 2:
                color=rviz_visual_tools::BLUE;
                break;
            case 3:
                color=rviz_visual_tools::GREEN;
                break;
            default:
                ;
        }
        
        double draw_magnification = 1;
        publishSolution_single(rviz, sol, color, agent_name, draw_magnification, first_planned);
        //publishSolution(sol,color,agent_name,first_planned);
        return true;
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::agent_callback(const mrfert::locking::ConstPtr& msg)
{
    if (msg->priority<priority &&
	dist(msg->x,msg->y,x,y) < (2.0*ARC_MAX_LENGTH / map_scale * graph_scale) )
    {
        std::unique_lock<std::mutex> lk(collision_mutex);
        last_locking_received[msg->priority]=*msg;
        for (auto my_n:lck.occupied_nodes)
        {
            auto my_average= my_n.t_start + (my_n.t_end-my_n.t_start)*0.5;
            for (auto other_n:msg->occupied_nodes)
	    {
//                 if (fabs(my_n.x-other_n.x)<0.01 && fabs(my_n.y-other_n.y)<0.01 && !(my_n.t_start>other_n.t_end || my_n.t_end<other_n.t_start )) 
                if (fabs(my_n.x-other_n.x)< 1 && fabs(my_n.y-other_n.y)< 1 && !(my_average>other_n.t_end || my_average<other_n.t_start )) 
                {
                    ROS_INFO_STREAM(agent_name<<": I got a collision in node (Graph srs) " << my_n.x << " " << my_n.y << " " << int(my_n.t_start.toSec())%1000);
                    this->collision=true;
                    return;
                }
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////
bool single_agent_planner::getNextNode(mrfert::node& node)
{
    ros::Time next_time;
    auto now=ros::Time::now();
    for (auto n:lck.occupied_nodes)
    {
        if ((n.t_start.toSec()+n.t_end.toSec())/2.0>=now.toSec())
        {
            next_time.fromSec((n.t_start.toSec()+n.t_end.toSec())/2.0);
            node=n;
            return true;
        }
    }
    return false;
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::init()
{
    bool planned=env->plan(start_x,start_y,global_x,global_y,2);//Dal prossimo nella lista, messo a tempo zero, ma tutti gli occupied devono essere traslati
    if (!planned) abort();
    env->getSolution(lck.occupied_nodes,ros::Time::now()+ros::Duration(10.0));
    last_updated=ros::Time::now()+ros::Duration(10.0);
    first_planned=ros::Time::now();
    updated=true;
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::run()
{
    /*
     * if check for collision
     *  replan
     *  send new plan
     *  update solution*/
    std::unique_lock<std::mutex> lk(collision_mutex);
    if (collision)
    {
        env->clearOccupiedMatrix();
        mrfert::node n;
        if(!getNextNode(n)) abort();
        //Mirko secondo me errore qui sotto, non torna l'offset giusto
        const auto offset= n.t_start+((n.t_end-n.t_start)*0.5);
        for (auto m:last_locking_received)
            if (m.first<this->priority)
                env->addOccupiedInfo(m.second.occupied_nodes,offset);
        bool planned=env->plan(n.x,n.y,global_x,global_y,0.7);//Dal prossimo nella lista, messo a tempo zero, ma tutti gli occupied devono essere traslati
        if(planned) 
        {
            lck.x=x;
            lck.y=y;
            lck.priority=priority;
            env->getSolution(lck.occupied_nodes,offset);
            last_updated=offset;
            send_locking_info(lck);
            updated=true;
            collision=false;
        }
    }
    alive.occupied_nodes.clear();
    alive.x=x;
    alive.y=y;
    alive.priority=priority;
//     std::cout<<alive<<std::endl;
    keep_alive.publish(alive);
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::setPosition(double x, double y)
{
    this->x=x/map_scale*graph_scale;
    this->y=y/map_scale*graph_scale;
    
    //std::cout << agent_name << " set position... " << this->x << " " << this->y << std::endl;
}

//////////////////////////////////////////////////////////////////////////
double single_agent_planner::dist(double x,double y, double x1, double y1)
{
    return sqrt(pow(x-x1,2)+pow(y-y1,2));
}

//////////////////////////////////////////////////////////////////////////
std::vector<geometry_msgs::Point> single_agent_planner::convert2DSolution(std::vector<node3D>& sol, ros::Time zero)
{
    std::vector<geometry_msgs::Point> res;
    auto now=zero;
    for (auto p:sol)
    {
        geometry_msgs::Point point;
        point.x=p.x;
        point.y=p.y;
        point.z=1*((p.t_start)+(p.t_end-p.t_start)*0.5).toSec();
//         point.z=1*((p.t_start-now)+(p.t_end-now)).toSec()/2.0;
	//std::cout<<agent_name<<" (Map srs) "<<point.x<<" "<<point.y<<" "<<((int)point.z)%1000<<std::endl;
        point.z=(int(point.z)%1000)*0.1;
        res.push_back(point);
    }
    return res;
}

//////////////////////////////////////////////////////////////////////////
void single_agent_planner::publishSolution(
    std::vector<node3D>& sol_, 
    rviz_visual_tools::colors color_,
    std::string name_,ros::Time zero)
{
    std::vector<geometry_msgs::Point> l_points = convert2DSolution(sol_,zero);
    rviz.publishPath(l_points, color_, rviz_visual_tools::XLARGE, name_+"arcs",1);
    int count = 0;
    int id=1;
    for(auto l_point:l_points)
    {
        rviz_visual_tools::colors l_sphereColor = color_;
        if(count == 0)
            l_sphereColor = rviz_visual_tools::colors::GREEN;
        else if (count == l_points.size()-1)
            l_sphereColor = rviz_visual_tools::colors::RED;
        
        geometry_msgs::Pose l_pose;
        l_pose.position=l_point;
        geometry_msgs::Vector3 l_scale; 
        rviz.publishSphere(l_pose, l_sphereColor, 0.5, name_+"nodes",id++);
        count++;
    }
    while (count<12)
    {
        geometry_msgs::Pose l_pose;
        l_pose.position.x=100;
        l_pose.position.y=100;
        l_pose.position.z=0;
        geometry_msgs::Vector3 l_scale; 
        rviz.publishSphere(l_pose, rviz_visual_tools::BLACK, 0.5, name_+"nodes",id++);
        count++;
    }
    geometry_msgs::Pose pose1;
    rviz.publishAxis(pose1);
    ros::Duration(0.5).sleep();
}