#include "mrfert/Plane2DEnvironment.h"
#include <fstream>

typedef ob::XYTStateSpace::StateType XYTState;

#define TIME_COLLISION_DEBUG
#undef TIME_COLLISION_DEBUG

Plane2DEnvironment::Plane2DEnvironment(
  graph_msgs::GeometryGraph graph, 
  double max_speed, 
  double maxTime, 
  double occupied,
  double maxWidth, 
  double maxHeight)
{
    this->graph=graph;
    this->maxWidth_ = maxWidth-1;
    this->maxHeight_ = maxHeight-1;
    this->maxTime_=maxTime;
    createPlanner(max_speed);
    this->occupied_=occupied;
}


Plane2DEnvironment::Plane2DEnvironment(
  const char *ppm_file, 
  graph_msgs::GeometryGraph graph, 
  double max_speed, 
  double maxTime,
  double occupied)
{
    this->graph=graph;
    bool ok = false;
    try
    {
        ppm_.loadFile(ppm_file);
        ok = true;
    }
    catch(ompl::Exception &ex)
    {
        OMPL_ERROR("Unable to load %s.\n%s, will not continue without XY information", ppm_file, ex.what());
    }
    if (ok)
    {
        this->maxWidth_ = ppm_.getWidth() - 1;
        this->maxHeight_ = ppm_.getHeight() - 1;
        this->maxTime_=maxTime;
        createPlanner(max_speed);
	this->occupied_=occupied;
    }
}

void Plane2DEnvironment::createPlanner(double max_speed)
{
    occupied_matrix.resize(maxWidth_);
    for (auto& row:occupied_matrix)
        row.resize(maxHeight_);
    boost::shared_ptr<ob::XYTStateSpace> cspace; cspace.reset(new ob::XYTStateSpace());
    ompl::base::RealVectorBounds b(3);
    b.setHigh(0,maxWidth_);
    b.setHigh(1,maxHeight_);
    b.setHigh(2,maxTime_);
    cspace->setBounds(b);
    cspace->setup();
    ss_.reset(new og::SimpleSetup(ob::StateSpacePtr(cspace)));
    Plane2DMotionValidator *motionValidator = new Plane2DMotionValidator(ss_->getSpaceInformation(),graph,max_speed);
    ss_->getSpaceInformation()->setMotionValidator(ob::MotionValidatorPtr(motionValidator));
    // set state validity checking for this space
    ss_->setStateValidityChecker(boost::bind(&Plane2DEnvironment::isStateValid, this, _1));
    ss_->getSpaceInformation()->setStateValidityCheckingResolution(1.0/cspace->getMaximumExtent());
    ss_->getProblemDefinition()->setIntermediateSolutionCallback(boost::bind(&Plane2DEnvironment::IntermediateSolutionCallback,this,_1,_2,_3));
    boost::shared_ptr<ob::TimeSpaceLengthOptimizationObjective> objective;
    objective.reset(new ob::TimeSpaceLengthOptimizationObjective(ss_->getSpaceInformation()));
    ss_->setOptimizationObjective(objective);
    auto pl_temp = new og::MRTstar(ss_->getSpaceInformation(),graph,max_speed);
    pl_.reset(pl_temp);
    ss_->setPlanner(pl_);
}

void Plane2DEnvironment::IntermediateSolutionCallback(const ob::Planner *, const std::vector< const ob::State * > & path, const ob::Cost cost)
{
    return;
    std::cout<<"intermediate cost:"<<cost<<std::endl;
    for (std::size_t i = 0 ; i < path.size() ; ++i)
    {
        const int w = std::min(maxWidth_,(int)path.at(i)->as<XYTState>()->get(0));
        const int h = std::min(maxHeight_,(int)path.at(i)->as<XYTState>()->get(1));
        const int t = std::min(maxTime_,(int)path.at(i)->as<XYTState>()->get(2));
        ompl::PPM::Color &c = ppm_.getPixel(h, w);
        c.red = 0;
        c.green = 255;
        c.blue = 0;
    }
    static int solutions=0;
    ppm_.saveFile(("temp"+std::to_string(solutions++)).c_str());
    
}

bool Plane2DEnvironment::plan(unsigned int start_row, unsigned int start_col, unsigned int goal_row, unsigned int goal_col,double time)
{
    if (!ss_)
        return false;
    ss_->clear();
    ob::ScopedState<> start(ss_->getStateSpace());
    start[0] = start_row;
    start[1] = start_col;
    start[2] = 0;
    ss_->setStartState(start);
    ob::GoalPtr goal;
    goal.reset(new Plane2DGoalRegion(ss_->getSpaceInformation(),goal_row,goal_col));
    ss_->setGoal(goal);
    if (ss_->getPlanner())
        ss_->getPlanner()->clear();
    last_called_solve = ros::Time::now();
    ss_->solve(time);
    const std::size_t ns = ss_->getProblemDefinition()->getSolutionCount();
//     OMPL_INFORM("Found %d solutions", (int)ns);
    if (ss_->haveSolutionPath())
    {
//                     ss_->simplifySolution();
        og::PathGeometric &p = ss_->getSolutionPath();
        //             ss_->getPathSimplifier()->simplifyMax(p);
        //             ss_->getPathSimplifier()->smoothBSpline(p);
        return true;
    }
    else
        return false;
}

void Plane2DEnvironment::recordSolution(std::ostream& os)
{
    if (!ss_ || !ss_->haveSolutionPath())
        return;
    og::PathGeometric &p = ss_->getSolutionPath();
    double distance=0;
    double delta=0;
    double oldx=(int)p.getState(0)->as<XYTState>()->get(0);
    double oldy=(int)p.getState(0)->as<XYTState>()->get(1);
    double oldt=(int)p.getState(0)->as<XYTState>()->get(2);
    for (std::size_t i = 1 ; i < p.getStateCount() ; ++i)
    {
        const int w = std::min(maxWidth_,(int)p.getState(i)->as<XYTState>()->get(0));
        const int h = std::min(maxHeight_,(int)p.getState(i)->as<XYTState>()->get(1));
        const double t = std::min(1.0*maxTime_,p.getState(i)->as<XYTState>()->get(2));
        delta=(w-oldx)*(w-oldx)+(h-oldy)*(h-oldy);
        oldx=w;
        oldy=h;
        distance+=sqrt(delta);
//         std::cout<<w<<" "<<h<<" "<<t<<" "<<sqrt(delta)*mapScale<<" "<<sqrt(delta)*mapScale/(t-oldt)<<" "<<std::endl;
        oldt=t;
        
    }
    std::cout<<"total distance "<<distance<<std::endl;
    std::cout<<"average speed "<<distance/(p.getState(p.getStateCount()-1)->as<XYTState>()->get(2)-p.getState(0)->as<XYTState>()->get(2))<<std::endl;
    os<<distance<<" "<<distance/(p.getState(p.getStateCount()-1)->as<XYTState>()->get(2)-p.getState(0)->as<XYTState>()->get(2))<<std::endl;
//     p.interpolate();
    if (ppm_.getPixels().size()>0)
    for (std::size_t i = 0 ; i < p.getStateCount() ; ++i)
    {
        const int w = std::min(maxWidth_,(int)p.getState(i)->as<XYTState>()->get(0));
        const int h = std::min(maxHeight_,(int)p.getState(i)->as<XYTState>()->get(1));
        const int t = std::min(maxTime_,(int)p.getState(i)->as<XYTState>()->get(2));
        ompl::PPM::Color &c = ppm_.getPixel(h, w);
        c.red = 255-t*2;
        c.green = 0;
        c.blue = t*2;
    }
}

void Plane2DEnvironment::save(const char *filename)
{
//     std::cout<<"saving"<<std::endl;
    if (!ss_)
        return;
    ppm_.saveFile(filename);
}

void Plane2DEnvironment::addOccupiedInfo(std::vector< node3D > nodes, ros::Time zero,std::string name)
{
    if (zero.toSec()==0) 
      std::cout<<"HELP"<<std::endl;//zero=ros::Time::now();
    
    std::cout<<name<<" adding occupied nodes with offset "<<int(zero.toSec())%1000<<std::endl;
    for (auto node:nodes)
    {
        std::cout<<"---- node (Graph srs) "<<node.x<<" "<<node.y<<" "<<int((node.t_start-zero).toSec())%1000<<std::endl;
        occupied_matrix[node.x][node.y].push_back( (node.t_start-zero).toSec() );
        occupied_matrix[node.x][node.y].push_back( (node.t_end-zero).toSec() );
    }
}

void Plane2DEnvironment::getSolution(std::vector< node3D >& nodes,ros::Time zero)
{
    if (!ss_ || !ss_->haveSolutionPath())
        return;
//     if (zero.toSec()==0) zero=ros::Time::now();
    if (zero.toSec()==0) std::cout<<"HELP"<<std::endl;//zero=ros::Time::now();
//     
    og::PathGeometric &p = ss_->getSolutionPath();
    nodes.clear();
    node3D n;
    n.x = (int)ss_->getProblemDefinition()->getStartState(0)->as<XYTState>()->getX();
    n.y = (int)ss_->getProblemDefinition()->getStartState(0)->as<XYTState>()->getY();
    n.t_start=zero+ros::Duration(0.1);
    n.t_end=zero+ros::Duration(0.2);
    nodes.push_back(n);
    for (std::size_t i = 1 ; i < p.getStateCount() ; ++i)
    {
        const int w = (int)p.getState(i)->as<XYTState>()->get(0);
        const int h = (int)p.getState(i)->as<XYTState>()->get(1);
        const double t = p.getState(i)->as<XYTState>()->get(2);
        n.x=w;
        n.y=h;
	
        n.t_start=zero+ros::Duration(std::max(t-occupied_,0.));
        n.t_end=zero+ros::Duration(t+occupied_);
        nodes.push_back(n);
    }
}


void Plane2DEnvironment::clearOccupiedMatrix()
{
    for (auto& row:occupied_matrix)
        for (auto& column: row)
            column.clear();
}

bool Plane2DEnvironment::isStateValid(const ob::State *state) const
{
    const double t = state->as<XYTState>()->getT();
    const int x = (int)state->as<XYTState>()->get(0);
    const int y = (int)state->as<XYTState>()->get(1);
    for (int i=0;i<occupied_matrix[x][y].size();i=i+2)
    {
//         std::cout<<"checking node for time constraint"<<x<<" "<<y<<std::endl;
        if (occupied_matrix[x][y][i]<t && occupied_matrix[x][y][i+1]>t)
        {
#ifdef TIME_COLLISION_DEBUG
            std::cout<<"found an invalid state: "<<x<<" "<<y<<" "<<t<<" "<<occupied_matrix[x][y][i]<<" "<<occupied_matrix[x][y][i+1]<<std::endl;
#endif
            return false;
        }
    }
    return true;
    const ompl::PPM::Color &c = ppm_.getPixel(x, y);
    return c.red > 127 && c.green > 127 && c.blue > 127;
}