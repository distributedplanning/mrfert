#include "mrfert/TimeSpaceLengthOptimizationObjective.h"
#include <mrfert/Plane2DMotionValidator.h>



ompl::base::TimeSpaceLengthOptimizationObjective::TimeSpaceLengthOptimizationObjective(const ompl::base::SpaceInformationPtr& si): PathLengthOptimizationObjective(si)
{
}


ompl::base::Cost ompl::base::TimeSpaceLengthOptimizationObjective::motionCost(const ompl::base::State* s1, const ompl::base::State* s2) const
{
    const int w1 = (int)s1->as<XYTState>()->getX();
    const int h1 = (int)s1->as<XYTState>()->getY();
    const double t1 = s1->as<XYTState>()->getT();
    const int w2 = (int)s2->as<XYTState>()->getX();
    const int h2 = (int)s2->as<XYTState>()->getY();
    const double t2 = s2->as<XYTState>()->getT();
    double deltaT=t2-t1;
    if (deltaT<0) return Cost(10000);
    double d=sqrt(pow(w1-w2,2)+pow(h1-h2,2));
    double speed = d/deltaT;
    return Cost(d+deltaT+stateCost(s1).value()+stateCost(s2).value());
}

ompl::base::Cost ompl::base::TimeSpaceLengthOptimizationObjective::stateCost(const ompl::base::State* s) const
{
    return Cost(s->as<XYTStateSpace::StateType>()->getT());
}

bool ompl::base::TimeSpaceLengthOptimizationObjective::isSatisfied(ompl::base::Cost c) const
{
    return false;
}
