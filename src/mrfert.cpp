#include "mrfert/MRTstar.h"
#include <mrfert/TimeSpaceStateSpace.h>
#include <mrfert/Plane2DMotionValidator.h>
#include "ompl/base/goals/GoalSampleableRegion.h"
#include "ompl/tools/config/SelfConfig.h"
#include "ompl/base/objectives/PathLengthOptimizationObjective.h"
#include "ompl/base/Goal.h"
#include "ompl/base/goals/GoalState.h"
#include <algorithm>
#include <limits>
#include <boost/math/constants/constants.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <graph_msgs/GeometryGraph.h>
#include <fstream>

#define SAMPLING_DEBUG //This will output stuff related to samples
#define GROWING_DEBUG  //This will output stuff related to the growing of the tree and pruning
#define FORCE_INTERMEDIATE //This will call intermediateSolutionCallback even when a solution is not found yet
#undef FORCE_INTERMEDIATE
#undef SAMPLING_DEBUG
#undef GROWING_DEBUG

ompl::geometric::MRTstar::MRTstar(const ompl::base::SpaceInformationPtr& si, graph_msgs::GeometryGraph graph, double max_speed) :
base::Planner(si, "MRTstar"),
graph(graph),
max_speed(max_speed),
goalBias_(0.05),
delayCC_(true),
debugStream(false),
lastGoalMotion_(NULL),
useTreePruning_(true),
bestCost_(std::numeric_limits<double>::quiet_NaN()),
iterations_(0u)
{
    specs_.approximateSolutions = true;
    specs_.optimizingPaths = true;
    specs_.canReportIntermediateSolutions = true;
    
    Planner::declareParam<double>("goal_bias", this, &MRTstar::setGoalBias, &MRTstar::getGoalBias, "0.:.05:1.");
    Planner::declareParam<bool>("delay_collision_checking", this, &MRTstar::setDelayCC, &MRTstar::getDelayCC, "0,1");
    
    addPlannerProgressProperty("iterations INTEGER",
                               boost::bind(&MRTstar::numIterationsProperty, this));
    addPlannerProgressProperty("best cost REAL",
                               boost::bind(&MRTstar::bestCostProperty, this));
}

ompl::geometric::MRTstar::~MRTstar()
{
    freeMemory();
}

void ompl::geometric::MRTstar::setup()
{
    Planner::setup();
    tools::SelfConfig sc(si_, getName());
    if (!si_->getStateSpace()->hasSymmetricDistance() || !si_->getStateSpace()->hasSymmetricInterpolate())
    {
        OMPL_WARN("%s requires a state space with symmetric distance and symmetric interpolation.", getName().c_str());
    }
    
    //Mirko TODO there is no way my stuff is going to work with a nearestK implementation
    if (!nn_)
    {
        //nn_.reset(tools::SelfConfig::getDefaultNearestNeighbors<Motion*>(this));
        nn_.reset(new ompl::NearestNeighborsLinear<Motion*>());
    }
    nn_->setDistanceFunction(boost::bind(&MRTstar::distanceFunction, this, _1, _2));
    
    //Setup fast graph accessor maps
    int i=0;
    predecessors.resize(graph.nodes.size());
    explored_tree.resize(graph.nodes.size());
    explored_nodes.resize(graph.nodes.size());
    for (auto node:graph.nodes)
    {
	//std::cout << node.x << " " << node.y << std::endl;
      
        xy_index[int(node.x)][int(node.y)]=i;
        for (auto target:graph.edges[i].node_ids)
        {
            predecessors[target].insert(i);
        }
        i++;
    }
    // Setup optimization objective
    //
    // If no optimization objective was specified, then default to
    // optimizing path length as computed by the distance() function
    // in the state space.
    if (pdef_)
    {
        if (pdef_->hasOptimizationObjective())
            opt_ = pdef_->getOptimizationObjective();
        else
        {
            OMPL_INFORM("%s: No optimization objective specified. Defaulting to optimizing path length for the allowed planning time.", getName().c_str());
            opt_.reset(new base::PathLengthOptimizationObjective(si_));
            
            // Store the new objective in the problem def'n
            pdef_->setOptimizationObjective(opt_);
        }
    }
    else
    {
        OMPL_INFORM("%s: problem definition is not set, deferring setup completion...", getName().c_str());
        setup_ = false;
    }
    
    // Set the bestCost_ and prunedCost_ as infinite
    bestCost_ = opt_->infiniteCost();
}

void ompl::geometric::MRTstar::clear()
{
    setup_ = false;
    Planner::clear();
    freeMemory();
    if (nn_)
        nn_->clear();
    
    lastGoalMotion_ = NULL;
    goalMotions_.clear();
    startMotions_.clear();
    
    iterations_ = 0;
    bestCost_ = base::Cost(std::numeric_limits<double>::quiet_NaN());

    explored_nodes.clear();
    explored_tree.clear();
    growing_nodes.clear();
    
}

ompl::base::PlannerStatus ompl::geometric::MRTstar::solve(const base::PlannerTerminationCondition &ptc)
{
    checkValidity();
    base::Goal                  *goal   = pdef_->getGoal().get();
    base::GoalSampleableRegion  *goal_s = dynamic_cast<base::GoalSampleableRegion*>(goal);
    
    bool symCost = opt_->isSymmetric();
    
    // Check if there are more starts
    if (pis_.haveMoreStartStates() == true)
    {
        // There are, add them
        while (const base::State *st = pis_.nextStart())
        {
            Motion *motion = new Motion(si_);
            si_->copyState(motion->state, st);
            motion->cost = opt_->identityCost();

            nn_->add(motion);
            startMotions_.push_back(motion);
            const int x = st->as<ompl::base::XYTStateSpace::StateType>()->getX();
            const int y = st->as<ompl::base::XYTStateSpace::StateType>()->getY();
            if (xy_index.count(x) && xy_index[x].count(y))
            {
                growing_nodes.insert(xy_index[x][y]);
                motion->index=xy_index[x][y];
                explored_tree[motion->index].push_back(motion);
                explored_nodes[motion->index]=true;
            }
            else
                return ompl::base::PlannerStatus::INVALID_START;
        }
        
    }
    // No else
    
    if (nn_->size() == 0)
    {
        OMPL_ERROR("%s: There are no valid initial states!", getName().c_str());
        return base::PlannerStatus::INVALID_START;
    }
    
//     OMPL_INFORM("%s: Starting planning with %u states already in datastructure", getName().c_str(), nn_->size());
    
    if ((useTreePruning_) && !si_->getStateSpace()->isMetricSpace())
        OMPL_WARN("%s: The state space (%s) is not metric and as a result the optimization objective may not satisfy the triangle inequality. "
        "You may need to disable pruning or rejection."
        , getName().c_str(), si_->getStateSpace()->getName().c_str());
    
    const base::ReportIntermediateSolutionFn intermediateSolutionCallback = pdef_->getIntermediateSolutionCallback();
    
    Motion *solution       = lastGoalMotion_;
    
    Motion *approximation  = NULL;
    double approximatedist = std::numeric_limits<double>::infinity();
    bool sufficientlyShort = false;
    
    Motion *rmotion        = new Motion(si_);
    base::State *rstate    = rmotion->state;
    base::State *xstate    = si_->allocState();
    
    std::vector<Motion*>       nbh;
    
    std::vector<base::Cost>    costs;
    std::vector<base::Cost>    incCosts;
    std::vector<std::size_t>   sortedCostIndices;
    
    std::vector<int>           valid;
    unsigned int               rewireTest = 0;
    unsigned int               statesGenerated = 0;
    
    if (solution)
        OMPL_INFORM("%s: Starting planning with existing solution of cost %.5f", getName().c_str(), solution->cost.value());
    
    // our functor for sorting nearest neighbors
    CostIndexCompare compareFn(costs, *opt_);
    
    while (ptc == false)
    {
        iterations_++;
#ifdef GROWING_DEBUG
        std::cout<<"growing nodes: "<<growing_nodes.size()<<std::endl;
        int sum=0;
        for (auto i:explored_tree)
            sum+=i.size();
        std::cout<<"explored tree: "<<sum<<std::endl;
        sum=0;
        for (auto i:explored_nodes)
            if (i) sum++;
        std::cout<<"explored nodes: "<<sum<<std::endl;
#endif
        bool sampled=false;
        // sample random state (with goal biasing)
        if(goalMotions_.size()==0)
        {
            if (growing_nodes.size()==0) {return ompl::base::PlannerStatus::INVALID_GOAL;}
            sampled=sampleGrowing(rstate);
        }
        else
        {
            if (rng_.uniform01() < goalBias_)
            {
                const double goalt=goalMotions_[0]->state->as<XYTState>()->getT();
                double t = rng_.uniformReal(0,goalt);
                rstate->as<XYTState>()->setXYZ(goalMotions_[0]->state->as<XYTState>()->getX(),goalMotions_[0]->state->as<XYTState>()->getY(),t);
            }
            else
                sampled=sampleRefine(rstate);
        }
        if (!sampled) continue;
        // find closest state in the tree
        Motion *nmotion=0;// = nn_->nearest(rmotion);
        //Mirko Let's try to easiest way first: search all the nodes in the explored tree, check if they can reach rmotion, pick the "nearest" one in the sense of 3D distance
        const int x = int(rstate->as<XYTState>()->getX());
        const int y = int(rstate->as<XYTState>()->getY());
        const int new_index = xy_index.at(x).at(y);
        double min_distance=10000000;
        #ifdef SAMPLING_DEBUG
        std::cout<<"the state index "<<new_index<<" has "<<predecessors[new_index].size()<<" predecessors"<<std::endl; 
        #endif
        const double t = rstate->as<XYTState>()->getT();
        
        for (auto node:predecessors[new_index])
        {
            if (explored_tree[node].size()>0) //There is at least a point which can be connected collision free in 2D
            {
                #ifdef SAMPLING_DEBUG
                std::cout<<"checking predecessor index "<<node<<" with "<<explored_tree[node].size()<<" motions"<<std::endl;
                #endif
                for (auto motion:explored_tree[node])
                {
                    const double time = motion->state->as<XYTState>()->getT();
                    if (t<time) //we cannot go back in time, drop
                    {
                        continue;
                    }
                    double d = si_->distance(motion->state, rstate);
                    if (d<min_distance) min_distance=d;
                    nmotion=motion;
                }
            }
        }
        
        if (nmotion==0) //Could not connect, drop
            continue;
        
        if (intermediateSolutionCallback && si_->equalStates(nmotion->state, rstate))
            continue;
        
        base::State *dstate = rstate;
        
        // Check if the motion between the nearest state and the state to add is valid
        if (si_->checkMotion(nmotion->state, dstate))
        {
            // create a motion
            Motion *motion = new Motion(si_);
            si_->copyState(motion->state, dstate);
            motion->index=new_index;
            motion->parent = nmotion;
            motion->incCost = opt_->motionCost(nmotion->state, motion->state);
            motion->cost = opt_->combineCosts(nmotion->cost, motion->incCost);
            
            // Find nearby neighbors of the new motion
            getNeighbors(motion, nbh);
            
            rewireTest += nbh.size();
            ++statesGenerated;
            
            // cache for distance computations
            //
            // Our cost caches only increase in size, so they're only
            // resized if they can't fit the current neighborhood
            if (costs.size() < nbh.size())
            {
                costs.resize(nbh.size());
                incCosts.resize(nbh.size());
                sortedCostIndices.resize(nbh.size());
            }
            
            // cache for motion validity (only useful in a symmetric space)
            //
            // Our validity caches only increase in size, so they're
            // only resized if they can't fit the current neighborhood
            if (valid.size() < nbh.size())
                valid.resize(nbh.size());
            std::fill(valid.begin(), valid.begin() + nbh.size(), 0);
            
            // Finding the nearest neighbor to connect to
            // By default, neighborhood states are sorted by cost, and collision checking
            // is performed in increasing order of cost
            if (delayCC_)
            {
                // calculate all costs and distances
                for (std::size_t i = 0 ; i < nbh.size(); ++i)
                {
                    incCosts[i] = opt_->motionCost(nbh[i]->state, motion->state);
                    costs[i] = opt_->combineCosts(nbh[i]->cost, incCosts[i]);
                }
                
                // sort the nodes
                //
                // we're using index-value pairs so that we can get at
                // original, unsorted indices
                for (std::size_t i = 0; i < nbh.size(); ++i)
                    sortedCostIndices[i] = i;
                std::sort(sortedCostIndices.begin(), sortedCostIndices.begin() + nbh.size(),
                          compareFn);
                
                // collision check until a valid motion is found
                //
                // ASYMMETRIC CASE: it's possible that none of these
                // neighbors are valid. This is fine, because motion
                // already has a connection to the tree through
                // nmotion (with populated cost fields!).
                for (std::vector<std::size_t>::const_iterator i = sortedCostIndices.begin();
                     i != sortedCostIndices.begin() + nbh.size(); ++i)
                {
                    if (nbh[*i] == nmotion || si_->checkMotion(nbh[*i]->state, motion->state))
                    {
                        motion->incCost = incCosts[*i];
                        motion->cost = costs[*i];
                        motion->parent = nbh[*i];
                        valid[*i] = 1;
                        break;
                    }
                    else valid[*i] = -1;
                }
            }
            // add motion to the tree
            nn_->add(motion);

            explored_tree[new_index].push_back(motion);
            explored_nodes[new_index]=true;
            growing_nodes.insert(new_index);
            motion->parent->children.push_back(motion);
            
            bool checkForSolution = false;
            for (std::size_t i = 0; i < nbh.size(); ++i)
            {
                if (nbh[i] != motion->parent)
                {
                    base::Cost nbhIncCost;
                    if (symCost)
                        nbhIncCost = incCosts[i];
                    else
                        nbhIncCost = opt_->motionCost(motion->state, nbh[i]->state);
                    base::Cost nbhNewCost = opt_->combineCosts(motion->cost, nbhIncCost);
                    if (opt_->isCostBetterThan(nbhNewCost, nbh[i]->cost))
                    {
                        bool motionValid;
                        if (valid[i] == 0)
                        {
                            motionValid = si_->checkMotion(motion->state, nbh[i]->state);
                        }
                        else
                        {
                            motionValid = (valid[i] == 1);
                        }
                        
                        if (motionValid)
                        {
                            // Remove this node from its parent list
                            removeFromParent (nbh[i]);
                            // Add this node to the new parent
                            nbh[i]->parent = motion;
                            nbh[i]->incCost = nbhIncCost;
                            nbh[i]->cost = nbhNewCost;
                            nbh[i]->parent->children.push_back(nbh[i]);
                            
                            // Update the costs of the node's children
                            updateChildCosts(nbh[i]);
                            
                            checkForSolution = true;
                        }
                    }
                }
            }
            
            // Add the new motion to the goalMotion_ list, if it satisfies the goal
            double distanceFromGoal;
            if (goal->isSatisfied(motion->state, &distanceFromGoal))//Mirko TODO where is this implemented? Is this useful?
            {
                goalMotions_.push_back(motion);
                checkForSolution = true;
            }
            
            
            //Mirko this code is forcing the callback to intermediate_solution, just for debugging stuff
#ifdef FORCE_INTERMEDIATE
            if (intermediateSolutionCallback && iterations_%100==0)
            {
                std::vector<const base::State *> spath;
                for (auto m:explored_tree)
                {
                    if (m.size())
                    {
                        spath.push_back(m.front()->state);
                    }
                }
                intermediateSolutionCallback(this, spath, bestCost_);
            }
#endif
            // Checking for solution or iterative improvement
            if (checkForSolution)
            {
                bool updatedSolution = false;
                for (size_t i = 0; i < goalMotions_.size(); ++i)
                {
                    if (opt_->isCostBetterThan(goalMotions_[i]->cost, bestCost_))
                    {
                        //New ompl if (opt_->isFinite(bestCost_) == false)
                        if (opt_->isCostBetterThan(bestCost_,opt_->infiniteCost()) == false)
                        {
                            OMPL_INFORM("%s: Found an initial solution with a cost of %.2f in %u iterations (%u vertices in the graph)", getName().c_str(), goalMotions_[i]->cost.value(), iterations_, nn_->size());
                            if (mat) (*mat)<<goalMotions_[i]->cost.value()<<" ";
                        }
                        bestCost_ = goalMotions_[i]->cost;
                        updatedSolution = true;
                    }
                    
                    sufficientlyShort = opt_->isSatisfied(goalMotions_[i]->cost);
                    if (sufficientlyShort)
                    {
                        solution = goalMotions_[i];
                        break;
                    }
                    else if (!solution ||
                        opt_->isCostBetterThan(goalMotions_[i]->cost,solution->cost))
                    {
                        solution = goalMotions_[i];
                        updatedSolution = true;
                    }
                }

                
                if (updatedSolution)
                {             
                    updateSamplingGraph();
                    if (useTreePruning_)
                    {
                        int size=pruneTree();
                        
                        Motion *intermediate_solution = solution->parent; // Do not include goal state to simplify code.
                        solution_path.clear();
                        //Push back until we find the start, but not the start itself
                        while (intermediate_solution->parent != NULL)
                        {
                            solution_path.push_back(intermediate_solution);
                            intermediate_solution = intermediate_solution->parent;
                        }
                        
//                         std::cout<<"pruned "<<size<<" motions using max time: "<<goal_time<<std::endl;
                    }
                    
                    if (intermediateSolutionCallback)
                    {
                        std::vector<const base::State *> spath;
                        Motion *intermediate_solution = solution->parent; // Do not include goal state to simplify code.
                        
                        //Push back until we find the start, but not the start itself
                        while (intermediate_solution->parent != NULL)
                        {
                            spath.push_back(intermediate_solution->state);
                            intermediate_solution = intermediate_solution->parent;
                        }
                        
                        intermediateSolutionCallback(this, spath, bestCost_);
                    }
                }
            }
            
            // Checking for approximate solution (closest state found to the goal)
            if (goalMotions_.size() == 0 && distanceFromGoal < approximatedist)
            {
                approximation = motion;
                approximatedist = distanceFromGoal;
            }
        }
        
        // terminate if a sufficient solution is found
        if (solution && sufficientlyShort)
            break;
    }
    
    bool approximate = (solution == NULL);
    bool addedSolution = false;
    if (approximate)
        solution = approximation;
    else
        lastGoalMotion_ = solution;
    
    if (solution != NULL)
    {
        ptc.terminate();
        // construct the solution path
        std::vector<Motion*> mpath;
        while (solution != NULL)
        {
            mpath.push_back(solution);
            solution = solution->parent;
        }
        
        // set the solution path
        PathGeometric *geoPath = new PathGeometric(si_);
        for (int i = mpath.size() - 1 ; i >= 0 ; --i)
            geoPath->append(mpath[i]->state);
        
        base::PathPtr path(geoPath);
        // Add the solution path.
        base::PlannerSolution psol(path);
        psol.setPlannerName(getName());
        if (approximate)
            psol.setApproximate(approximatedist);
        // Does the solution satisfy the optimization objective?
        psol.setOptimized(opt_, bestCost_, sufficientlyShort);
        pdef_->addSolutionPath(psol);
        
        addedSolution = true;
    }
    
    si_->freeState(xstate);
    if (rmotion->state)
        si_->freeState(rmotion->state);
    delete rmotion;
    
//     OMPL_INFORM("%s: Created %u new states. Checked %u rewire options. %u goal states in tree. Final solution cost %.3f", getName().c_str(), statesGenerated, rewireTest, goalMotions_.size(), bestCost_.value());
    if (mat) (*mat)<<bestCost_.value()<<" ";
    return base::PlannerStatus(addedSolution, approximate);
}

void ompl::geometric::MRTstar::getNeighbors(Motion *motion, std::vector<Motion*> &nbh) const
{
    //Mirko: We are using the graph, that's our best shot
    //But we should use the graph of the motion, not the 2D graph!!
    nbh.clear();
    for (auto node:predecessors[motion->index])
    {
//         std::cout<<"added to neighbors node "<<node<<std::endl;
        for (auto m:explored_tree[node])
        {
            nbh.push_back(m);
//             std::cout<<m<<std::endl;
        }
    }
    return;
}

void ompl::geometric::MRTstar::removeFromParent(Motion *m)
{
    for (std::vector<Motion*>::iterator it = m->parent->children.begin ();
         it != m->parent->children.end (); ++it)
         {
             if (*it == m)
             {
                 m->parent->children.erase(it);
                 break;
             }
         }
}

void ompl::geometric::MRTstar::updateChildCosts(Motion *m)
{
    for (std::size_t i = 0; i < m->children.size(); ++i)
    {
        m->children[i]->cost = opt_->combineCosts(m->cost, m->children[i]->incCost);
        updateChildCosts(m->children[i]);
    }
}

bool ompl::geometric::MRTstar::checkAndEraseGrowingNode(int i)
{
    int counter=graph.edges.at(i).node_ids.size();
    for (auto node:graph.edges.at(i).node_ids)
    {
        if (explored_nodes[node]) counter--;
    }
    if (counter==0)
    {
        growing_nodes.erase(i);
#ifdef GROWING_DEBUG
        std::cout<<"removed from growing nodes "<<i<<std::endl;
#endif
        return false;
    }
    return true;
}

double dist(int x,int y,int x1,int y1)
{
    return sqrt(pow(x-x1,2)+pow(y-y1,2));
}


bool ompl::geometric::MRTstar::keepCondition(const Motion* motion, double maxTime) const
{
    return motion->state->as<XYTState>()->getT()<maxTime+0.001;
}

ompl::geometric::MRTstar::Motion* ompl::geometric::MRTstar::getBestGoalMotion()
{
    Motion* bestGoal=goalMotions_[0];
    double bestTime=goalMotions_[0]->state->as<XYTState>()->getT();
    for (auto goal:goalMotions_)
    {
        if (goal->state->as<XYTState>()->getT()<bestTime)
        {
            bestGoal=goal;
            bestTime=goal->state->as<XYTState>()->getT();
        }
    }
    return bestGoal;
}

int ompl::geometric::MRTstar::pruneTree()
{
    // The number pruned
    int numPruned = 0;
    // We are only pruning motions if they, AND all descendents, have a estimated time greater than maxTime
    // The easiest way to do this is to find leaves that should be pruned and ascend up their ancestry until a motion is found that is kept.
    // To avoid making an intermediate copy of the NN structure, we process the tree by descending down from the start(s).
    // In the first pass, all Motions with a cost below pruneTreeCost, or Motion's with children with costs below pruneTreeCost are added to the replacement NN structure,
    // while all other Motions are stored as either a 'leaf' or 'chain' Motion. After all the leaves are disconnected and deleted, we check
    // if any of the the chain Motions are now leaves, and repeat that process until done.
    // This avoids (1) copying the NN structure into an intermediate variable and (2) the use of the expensive NN::remove() method.
    
    //Mirko I assume the above stuff makes really sense, but my pruning method is way simpler, and I also have the following assumption that works:
    // if a motion has a time greater than maxTime, all its children will have a time greater than maxTime, meaning that I can erase all of them!
    // Variable

    // The queue of Motions to process:
    std::queue<Motion*, std::deque<Motion*> > motionQueue;
    // The list of leaves to prune
    std::queue<Motion*, std::deque<Motion*> > leavesToPrune;
    // The list of chain vertices to recheck after pruning
    std::list<Motion*> chainsToRecheck;
    
    Motion* bestGoal=getBestGoalMotion();
    double maxTime=bestGoal->state->as<XYTState>()->getT();
    
    //Clear the NN structure:
    nn_->clear();
            
    // Put all the starts into the NN structure and their children into the queue:
    // We do this so that start states are never pruned.
    for (unsigned int i = 0u; i < startMotions_.size(); ++i)
    {
        // Add to the NN
        nn_->add(startMotions_.at(i));
        
        // Add their children to the queue:
        addChildrenToList(&motionQueue, startMotions_.at(i));
    }
    
    while(motionQueue.empty() == false)
    {
        if (keepCondition(motionQueue.front(),maxTime))
        {
            nn_->add(motionQueue.front());
            
            addChildrenToList(&motionQueue, motionQueue.front());
        }
        else
        {
            leavesToPrune.push(motionQueue.front());
        }
        // Pop the iterator
        motionQueue.pop();
    }
    
    
    // We now have a list of Motions to definitely remove, and a list of Motions to recheck
    // Iteratively check the two lists until there is nothing to to remove
    while (leavesToPrune.empty() == false)
    {
        // First empty the leave-to-prune
        while (leavesToPrune.empty() == false)
        {
            // Remove the leaf from its parent
            removeFromParent(leavesToPrune.front());
            
            //Add children of the leaf to be removed
            addChildrenToList(&leavesToPrune, leavesToPrune.front());
            
            // Erase the actual motion
            
            //First search for the graph fast accessor and remove them
            int index=xy_index.at(leavesToPrune.front()->state->as<XYTState>()->getX()).at(leavesToPrune.front()->state->as<XYTState>()->getY());
//             explored_tree[index].remove(leavesToPrune.front());
            int counter=0;
            for (auto it=explored_tree[index].begin();it!=explored_tree[index].end();)
            {
                if (*it==leavesToPrune.front())
                {
                    it=explored_tree[index].erase(it);
                }
                else
                    ++it;
                if (counter>1000) abort();
                counter++;
            }

            if (explored_tree[index].size()==0)
            {
                explored_nodes[index]=false;
            }
            // First free the state
            si_->freeState(leavesToPrune.front()->state);
            
            // then delete the pointer
            delete leavesToPrune.front();
            
            // And finally remove it from the list, erase returns the next iterator
            leavesToPrune.pop();
            
            // Update our counter
            ++numPruned;
        }
        
    }
    
    //Finally, there will be only one goal motion left, since all the others were with a worse cost
    goalMotions_.clear();
    goalMotions_.push_back(bestGoal);
        
    // All done pruning.
    return numPruned;
}


void ompl::geometric::MRTstar::updateSamplingGraph()
{
    informedSamplingGraph.clear();
    const int startx=startMotions_[0]->state->as<XYTState>()->getX();
    const int starty=startMotions_[0]->state->as<XYTState>()->getY();
    const double startt=startMotions_[0]->state->as<XYTState>()->getT();
    double goalt=getBestGoalMotion()->state->as<XYTState>()->getT();
    const int goalx=goalMotions_[0]->state->as<XYTState>()->getX();
    const int goaly=goalMotions_[0]->state->as<XYTState>()->getY();
    double deltaT = goalt-startt;
    double maxDistance= deltaT*max_speed;
    
    for (int i=0;i<graph.nodes.size();i++)
    {
        int x=graph.nodes[i].x;
        int y=graph.nodes[i].y;
        if(dist(x,y,startx,starty)+dist(x,y,goalx,goaly)<maxDistance)
        {
            informedSamplingGraph.push_back(i);
        }
    }
//     std::cout<<"informed sampling size: "<<informedSamplingGraph.size()<<std::endl;
}

bool ompl::geometric::MRTstar::sampleRefine(base::State* rstate)
{
    int counter=0;
    const int startx=startMotions_[0]->state->as<XYTState>()->getX();
    const int starty=startMotions_[0]->state->as<XYTState>()->getY();
    const double startt=startMotions_[0]->state->as<XYTState>()->getT();
    double goalt=getBestGoalMotion()->state->as<XYTState>()->getT();
    const int goalx=goalMotions_[0]->state->as<XYTState>()->getX();
    const int goaly=goalMotions_[0]->state->as<XYTState>()->getY();
    double deltaT = goalt-startt;
    double maxDistance= deltaT*max_speed;
    while(true)
    {
        counter++;
        if (rng_.uniform01() < goalBias_ && solution_path.size()>2)
        {
            int i = rng_.uniformInt(0,solution_path.size()-2); //avoid sampling the goal
            auto it=solution_path.begin();
            std::advance(it,i);
            const int x = (*it)->state->as<XYTState>()->getX();
            const int y = (*it)->state->as<XYTState>()->getY();
            double mintime=(*it)->parent->state->as<XYTState>()->getT();
            std::advance(it,1);//there is always one left, since the goal was not in the sample size
            double maxtime=(*it)->state->as<XYTState>()->getT();
            double time=rng_.uniformReal(mintime,maxtime);
	    rstate->as<XYTState>()->setXYZ(x,y,time);
	    
	    if (!si_->isValid(rstate))
	    {
		continue;
	    }
        	    
            return true;
        }
        else 
        {
            int node = sampleFromContainer<std::vector<int>,int>(informedSamplingGraph);
            const int x=graph.nodes[node].x;
            const int y=graph.nodes[node].y;
            
            for (auto prev:predecessors[node])
            {
                if (explored_nodes[prev])
                {
                    if(dist(x,y,startx,starty)+dist(x,y,goalx,goaly)<maxDistance)
                    {
                        double mintime=startt+dist(x,y,startx,starty)/max_speed;
                        double maxtime=goalt-dist(x,y,goalx,goaly)/max_speed;
                        
                        double time=rng_.uniformReal(mintime,maxtime);
                        rstate->as<XYTState>()->setXYZ(x,y,time);
			
			if (!si_->isValid(rstate))
			{
			    continue;
			}
			
                        return true;
                    }
                    else
                    {
    //                     std::cout<<"very strange, we sampled an informed node but it was bad!!"<<std::endl;
                    }
                }
            }
        }
        if(counter>10000) return false;
    }
    
}

bool ompl::geometric::MRTstar::sampleGrowing(base::State* rstate)
{
    // Attempt to generate a sample, if we fail (e.g., too many rejection attempts), skip the remainder of this loop and return to try again
    bool found=false;
    int counter=0;
    while(!found)
    {
        counter++;
        if (counter==1000) return false;
        if (growing_nodes.size()==0)
        {
            std::cout<<"no more growing nodes!!"<<std::endl;
            return false;
        }
        #ifdef SAMPLING_DEBUG 
        std::cout<<"growing nodes: "<<growing_nodes.size()<<std::endl;
        #endif
        //Pick one of the growing nodes
        int i=sampleFromContainer<std::set<int>,int>(growing_nodes);
        if (!checkAndEraseGrowingNode(i)) continue;
        #ifdef SAMPLING_DEBUG 
        std::cout<<"selected node "<<i<<std::endl; 
        #endif
        if (!sampleGrowingUniformNear2D(rstate,i)) continue;
        found = true;
    }
    return true;
}


bool ompl::geometric::MRTstar::sampleGrowingUniformNear2D(base::State *statePtr,int graph2DnodeIndex)
{
    #ifdef SAMPLING_DEBUG 
    std::cout<<"the selected node "<<graph2DnodeIndex<<" has been explored "<<explored_tree[graph2DnodeIndex].size()<<" times"<<std::endl; 
    #endif
    
    //Sample a motion in order to get a time
    Motion* motion=sampleFromContainer<std::list<Motion*>,Motion*>(explored_tree[graph2DnodeIndex]);
    #ifdef SAMPLING_DEBUG
    std::cout<<"the motion selected "<<graph2DnodeIndex<<" is x: "<<graph.nodes[graph2DnodeIndex].x<<" y:"<<graph.nodes[graph2DnodeIndex].y<<" t:"<<motion->state->as<XYTState>()->getT()<<std::endl;
    #endif
    
    //Sample a non-explored node from graph2DnodeIndex
    std::vector<int> good;
    for (auto node:graph.edges.at(graph2DnodeIndex).node_ids)
    {
        if (explored_nodes.at(node)) continue;
        good.push_back(node);
    }
    if (good.size()==0) 
    {
        std::cout<<"all nodes from "<<graph2DnodeIndex<<" are already explored"<<std::endl;
        return false;
    }
    int index = sampleFromContainer<std::vector<int>,int>(good);
    
    //Create the state by adding the time
    statePtr->as<XYTState>()->setX(round(graph.nodes.at(index).x));
    statePtr->as<XYTState>()->setY(round(graph.nodes.at(index).y));
    double l_distance = computeDistanceBetweenNodes( graph.nodes[graph2DnodeIndex].x, graph.nodes[graph2DnodeIndex].y, round(graph.nodes.at(index).x), round(graph.nodes.at(index).y) );
    double l_min_time = l_distance/max_speed;
    const double t1=motion->state->as<XYTState>()->getT();
    bool found=false;
    int counter=0;
    int MAX_COUNTER = 1000;
    while (!found)
    {
	double l_delay = ( 1 - rng_.halfNormalReal( 0, 1, 5 ) ) * l_min_time;
        l_delay += rng_.uniformReal(0, ((double)counter)/20 );

	double total= t1 + l_min_time + l_delay;
        if ( total > this->si_->getStateSpace()->as<ompl::base::XYTStateSpace>()->getBounds().high[2] ) 
            total = this->si_->getStateSpace()->as<ompl::base::XYTStateSpace>()->getBounds().high[2];
        if (total<0)
            total=0;
        statePtr->as<XYTState>()->setT(total);
        if (si_->isValid(statePtr))
        {
            found=true;
            break;
        }
        counter++;
        if (counter>MAX_COUNTER) return false;
    }
    #ifdef SAMPLING_DEBUG
    std::cout<<"the sampled is "<<index<<" is x: "<<statePtr->as<XYTState>()->getX()<<" y:"<<statePtr->as<XYTState>()->getY()<<" t:"<<statePtr->as<XYTState>()->getT()<<std::endl;
#endif
    return true;
}


void ompl::geometric::MRTstar::freeMemory()
{
    if (nn_)
    {
        std::vector<Motion*> motions;
        nn_->list(motions);
        for (std::size_t i = 0 ; i < motions.size() ; ++i)
        {
            if (motions[i]->state)
            {
                si_->freeState(motions[i]->state);
            }
            delete motions[i];
        }
    }
}

void ompl::geometric::MRTstar::getPlannerData(base::PlannerData &data) const
{
    Planner::getPlannerData(data);
    
    std::vector<Motion*> motions;
    if (nn_)
        nn_->list(motions);
    
    if (lastGoalMotion_)
        data.addGoalVertex(base::PlannerDataVertex(lastGoalMotion_->state));
    
    for (std::size_t i = 0 ; i < motions.size() ; ++i)
    {
        if (motions[i]->parent == NULL)
            data.addStartVertex(base::PlannerDataVertex(motions[i]->state));
        else
            data.addEdge(base::PlannerDataVertex(motions[i]->parent->state),
                         base::PlannerDataVertex(motions[i]->state));
    }
}

void ompl::geometric::MRTstar::addChildrenToList(std::queue<Motion*, std::deque<Motion*> > *motionList, Motion* motion)
{
    for (unsigned int j = 0u; j < motion->children.size(); ++j)
    {
        motionList->push(motion->children.at(j));
    }
}


double ompl::geometric::MRTstar::computeDistanceBetweenNodes(double x, double y, double x1, double y1)
{
    return sqrt(pow(x-x1,2)+pow(y-y1,2));
}