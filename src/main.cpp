#include <ros/package.h>
#include <ros/init.h>
#include <ros/node_handle.h>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <gmlreader/graphmsgs_wrapper.hpp>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <mrfert/Plane2DEnvironment.h>

#include <mrfert/single_agent_planner.h>


typedef std::vector<node3D> Path;
 
double speed;
double max_time;
double map_scale;
double graph_scale;
double visualization_scale_factor = 1./10.;

geometry_msgs::Point convert2DPoint(node3D & node_, double const& map_scale_)
{
  auto now=ros::Time(0);//ros::Time::now();
  geometry_msgs::Point point;

  point.x=node_.x*map_scale_;
  point.y=node_.y*map_scale_;
  double ha = ((node_.t_start-now) + (node_.t_end-now)).toSec();
  auto dur = (node_.t_start-now);
  
  point.z= visualization_scale_factor * ((node_.t_start-now) + (node_.t_end-now)).toSec() / 2.0 ;
  //std::cout<<point.x<<" "<<point.y<<" "<<point.z<<std::endl;
  return point;
}

std::vector<geometry_msgs::Point> convert2DSolution(std::vector<node3D>& sol, double const& map_scale_)
{
    std::vector<geometry_msgs::Point> res;
    
    for (auto p:sol)
      res.push_back(convert2DPoint(p, map_scale_));
    return res;
}

graph_msgs::GeometryGraph convert2Graph(std::vector<node3D>& sol_, double const& map_scale_)
{
  graph_msgs::GeometryGraph l_graph;
  
  std::vector<geometry_msgs::Point> l_points = convert2DSolution(sol_, map_scale_);
  l_graph.nodes = l_points;
  for(size_t i=0; i < l_graph.nodes.size(); ++i)
  {
    graph_msgs::Edges edgeContainer;
    if(i != l_graph.nodes.size()-1)
      edgeContainer.node_ids.push_back(i+1);
    l_graph.edges.push_back(edgeContainer);
  }
    
  return l_graph;
}

void publishSolution(
  rviz_visual_tools::RvizVisualTools& rviz,
  std::vector<node3D>& sol_, 
  rviz_visual_tools::colors color_,
  std::string name_,
  double const& map_scale_)
{
  std::vector<geometry_msgs::Point> l_points = convert2DSolution(sol_, map_scale_);
  rviz.publishPath(l_points, color_, rviz_visual_tools::REGULAR, name_);
  rviz.publishGraph(convert2Graph(sol_,map_scale_), color_, map_scale_); 
  
  
  auto now=ros::Time(0);//ros::Time::now();
  int count = 0;
  rviz_visual_tools::colors l_sphereColor = color_;
  for(auto l_node:sol_)
  {
    geometry_msgs::Point l_point = convert2DPoint(l_node, map_scale_);

    geometry_msgs::Pose l_pose;
    l_pose.position=l_point;
    geometry_msgs::Vector3 l_scale; 
    //rviz.publishSphere(l_pose, l_sphereColor, 0.5);

    double l_h = 1.*visualization_scale_factor * ((l_node.t_end-now) - (l_node.t_start-now)).toSec() / 2. ;
        
    if( (count == 0) || (count == sol_.size()-1) )
      rviz.publishSphere(l_pose, l_sphereColor, map_scale_);
    else
      rviz.publishCylinder(l_pose, l_sphereColor, l_h, map_scale_); 
    
    count++;
  }
}

void publishGraph(
  rviz_visual_tools::RvizVisualTools& rviz,
  graph_msgs::GeometryGraph const& graph_,
  rviz_visual_tools::colors color_,
  double const& map_scale_)
{
  rviz.publishGraph(graph_, color_, map_scale_);
  
  for(auto l_node:graph_.nodes)
  {
    geometry_msgs::Point l_point = l_node;

    geometry_msgs::Pose l_pose;
    l_pose.position=l_point;
    geometry_msgs::Vector3 l_scale; 
    rviz.publishSphere(l_pose, color_, map_scale_);
  }
  
}

struct INode
{
  double x;
  double y;
  
  inline void scale(double s) {x *=s; y *=s;}
  
  void init(std::vector<geometry_msgs::Point> const& graph, int node)
  {
    x = graph.at(node).x;
    y = graph.at(node).y;
  }
};

int correct_y_coord(int const& value)
{
  return value;
  const int l_max_value = 2000;
  return l_max_value-value;
}

int main(int argc, char ** argv)
{
    //Hello
    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;
    ros::init(argc,argv,"mrfert_node");
    std::string path=ros::package::getPath("mrfert");
    ros::NodeHandle nh("~");

    //Load ros parameters
    std::string filename;
    if (!nh.getParam("max_speed",speed))
    {
        std::cout<<"please specify a max_speed parameter, using default as "<<2.0<<std::endl;
        speed = 2.0;
    }
    if (!nh.getParam("max_time",max_time))
    {
        std::cout<<"please specify a max_time parameter, using default as "<<500<<std::endl;
        max_time = 500;
    }
    if (!nh.getParam("map_scale",map_scale))
    {
        std::cout<<"please specify a map_scale parameter, using default as "<<0.2<<std::endl;
        map_scale = 0.02;
    }
    if (!nh.getParam("filename",filename))
    {
        std::cout<<"please specify a filename parameter for *.gml and *.ppm, using default as "<<"floor"<<std::endl;
        filename="floor";
    }
    if (!nh.getParam("graph_scale",graph_scale))
    {
        std::cout<<"please specify a graph_scale parameter, using default as "<<1.<<std::endl;
        graph_scale=.1;
    }
    
    double occupied_node_time;
    std::string temp_name;
    if (nh.searchParam("occupied_node_time",temp_name))
    {
        nh.getParam(temp_name,occupied_node_time);
	//std::cout<<" --- SingleAgent --- " << agent_name << " occupied_time parameter, using "<<occupied_node_time<<std::endl;
    }
    else
    {
        occupied_node_time = 5;
        ROS_WARN_STREAM("please specify a occupied_node_time parameter, using default as "<<occupied_node_time);
    }
    
    //Load graph nodes
    graph_msgs::GeometryGraph graph;
    std::ifstream fs(path+"/"+filename+".gml");
    graphmsgs_gml reader;
    reader.read(fs,graph, graph_scale);

//     //Make arcs uniques (just in case voronoi created multiple arcs between the same nodes)
// if(0)
// {
//     for (int i=0;i<graph.edges.size();i++)
//     {
//         std::sort(graph.edges[i].node_ids.begin(),graph.edges[i].node_ids.end());
//         graph.edges[i].node_ids.erase(std::unique(graph.edges[i].node_ids.begin(),graph.edges[i].node_ids.end()),graph.edges[i].node_ids.end());
//     }
// }
    
    //Load xy for each node, just to check that start and goal node exist
    std::map<int,std::map<int,int>> xy_index;
    int i=0;
    for (auto& node:graph.nodes)
    {
	//Change of reference frame because voronoi works with screen coordinates, while gml uses robot coordinates
        node.y=correct_y_coord(node.y);
	
	std::cout << "[" << i << " rev]:"<< node.x << " " << node.y <<std::endl;
		
        xy_index[(int)round(node.x)][(int)round(node.y)]=i;
        i++;
    }
    std::cout << "End of Node list" <<std::endl;
    
    std::ofstream mat("out.mat", std::ofstream::app);
    
    //Load the environment, with the ppm, graph and other parameters
    Plane2DEnvironment env(
      graph, 
      speed/map_scale*graph_scale, 
      max_time, 
      occupied_node_time);
    
    env.setDebugStream(&mat);
    ompl::RNG rng;
    double time = rng.uniformInt(1,20)/2.0;
    //Near nodes: 249, 303, 262, 332
    //Far nodes: 254, 334, 386, 280
    //269 165   312 220
    
    INode start1, end1, start2, end2, start3, end3, start4, end4;
    
    // Centro Piaggio
//     start1.init(graph.nodes, 4-1);
//     start2.init(graph.nodes, 18-1);
//     start3.init(graph.nodes, 68-1);
//     end1.init(graph.nodes, 63-1);
//     end2.init(graph.nodes, 42-1);
//     end3.init(graph.nodes, 54-1);
 
    // Cross
//     start1.init(graph.nodes, 8);
//     start2.init(graph.nodes, 7);
//     start3.init(graph.nodes, 6);
//     start4.init(graph.nodes, 5);
// 
//     end1.init(graph.nodes, 0);
//     end2.init(graph.nodes, 1);
//     end3.init(graph.nodes, 3);
//     end4.init(graph.nodes, 4);

//     Round
      start1.init(graph.nodes, 0);
      start2.init(graph.nodes, 8);
      start3.init(graph.nodes, 10);
      end1.init(graph.nodes, 9);
      end2.init(graph.nodes, 1);
      end3.init(graph.nodes, 11);
    
      int id=xy_index.at(start1.x).at(start1.y);
      id=xy_index.at(start2.x).at(start2.y);
      id=xy_index.at(start3.x).at(start3.y);
      id=xy_index.at(end1.x).at(end1.y);
      id=xy_index.at(end2.x).at(end2.y);
      id=xy_index.at(end3.x).at(end3.y);
    
    std::vector<node3D> sol1,sol2,sol3,sol4;
// #ifdef mirko
    //Finally, plan
    if (env.plan(start1.x, start1.y, end1.x, end1.y,time))//TODO: dear future mirko, if you change these values, the starting state will not be a node, thus you will need some more extra code or a fallback with a collision checking
    {
        mat<<time<<" ";
        env.recordSolution(mat);
        env.getSolution(sol1);
    }
//     env.save("result_demo.ppm");
//     std::cout << "saved" << std::endl;
    env.addOccupiedInfo(sol1);
    //Finally, plan
    if (env.plan(start2.x, start2.y, end2.x, end2.y,time))//TODO: dear future mirko, if you change these values, the starting state will not be a node, thus you will need some more extra code or a fallback with a collision checking
    {
        mat<<time<<" ";
        env.recordSolution(mat);
        env.getSolution(sol2);
    }
//     env.save("result_demo1.ppm");
    
    env.addOccupiedInfo(sol2);
// #endif
    //Finally, plan
    if (env.plan(start3.x, start3.y, end3.x, end3.y,time))//TODO: dear future mirko, if you change these values, the starting state will not be a node, thus you will need some more extra code or a fallback with a collision checking
    {
        mat<<time<<" ";
        env.recordSolution(mat);
        env.getSolution(sol3);
    }
    
    env.addOccupiedInfo(sol3);
// #endif
    //Finally, plan
    if (env.plan(start4.x, start4.y, end4.x, end4.y,time))//TODO: dear future mirko, if you change these values, the starting state will not be a node, thus you will need some more extra code or a fallback with a collision checking
    {
        mat<<time<<" ";
        env.recordSolution(mat);
        env.getSolution(sol4);
    }
    
//     env.save("result_demo2.ppm");
    ros::NodeHandle n;
    my_rviz_visual_tools r("map","/test");
//     sleep(1);
    ros::AsyncSpinner s(1);
    s.start();
    i=0;
    
    for (auto& n:graph.nodes)
    {
	n.x*=map_scale;
	n.y*=map_scale;
        n.z=0;
    }
    publishGraph(r, graph, rviz_visual_tools::BLUE, visualization_scale_factor);
    
    if (1)
    {
      publishSolution(r, sol1, rviz_visual_tools::MAGENTA, "path1", map_scale);
      publishSolution(r, sol2, rviz_visual_tools::LIME_GREEN, "path2", map_scale);
      publishSolution(r, sol3, rviz_visual_tools::CYAN, "path3", map_scale);
      publishSolution(r, sol4, rviz_visual_tools::YELLOW, "path3", map_scale);
    }
        
    geometry_msgs::Pose pose1;
    r.publishAxis(pose1);
    ros::Duration(0.5).sleep();
    mat.close();
    return 0;
}
