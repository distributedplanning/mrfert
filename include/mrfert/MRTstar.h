/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Rice University
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Rice University nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Authors: Alejandro Perez, Sertac Karaman, Ryan Luna, Luis G. Torres, Ioan Sucan, Javier V Gomez, Jonathan Gammell */

#ifndef OMPL_GEOMETRIC_PLANNERS_RRT_MRTSTAR_
#define OMPL_GEOMETRIC_PLANNERS_RRT_MRTSTAR_

#include "ompl/geometric/planners/PlannerIncludes.h"
#include "ompl/base/OptimizationObjective.h"
#include "ompl/datastructures/NearestNeighbors.h"
#include "Plane2DStateSampler.h"

#include <limits>
#include <list>
#include <vector>
#include <queue>
#include <deque>
#include <utility>
#include <iostream>
#include <fstream>
#include <graph_msgs/Edges.h>
#include <graph_msgs/GeometryGraph.h>


namespace ompl
{
    
    namespace geometric
    {
        
        /**
         *           @anchor gMRTstar
         *           @par Short description
         *           \ref gMRTstar "RRT*" (optimal RRT) is an asymptotically-optimal incremental
         *           sampling-based motion planning algorithm. \ref gMRTstar "RRT*" algorithm is
         *           guaranteed to converge to an optimal solution, while its
         *           running time is guaranteed to be a constant factor of the
         *           running time of the \ref gRRT "RRT". The notion of optimality is with
         *           respect to the distance function defined on the state space
         *           we are operating on. See ompl::base::Goal::setMaximumPathLength() for
         *           how to set the maximally allowed path length to reach the goal.
         *           If a solution path that is shorter than ompl::base::Goal::getMaximumPathLength() is
         *           found, the algorithm terminates before the elapsed time.
         *           @par External documentation
         *           S. Karaman and E. Frazzoli, Sampling-based
         *           Algorithms for Optimal Motion Planning, International Journal of Robotics
         *           Research, Vol 30, No 7, 2011.
         *           http://arxiv.org/abs/1105.1186
         */
        
        /** \brief Optimal Rapidly-exploring Random Trees */
        class MRTstar : public base::Planner
        {
        public:
            
            /**
             * @brief Constructor
             * 
             * @param si standard for all OMPL planners
             * @param graph 2D voronoi graph
             * @param max_speed maximum speed allowed for the robot, note that it is in pixel/s, not in meters!
             */
            MRTstar(const ompl::base::SpaceInformationPtr& si, graph_msgs::GeometryGraph graph,double max_speed);
            
            virtual ~MRTstar();
            
            virtual void getPlannerData(base::PlannerData &data) const;
            
            virtual base::PlannerStatus solve(const base::PlannerTerminationCondition &ptc);
            
            virtual void clear();
            
            virtual void setup();
            
            void setDebugStream(std::ostream* mat)
            {
                this->mat=mat;
                debugStream=true;
            }
            
            /** \brief Set the goal bias
             * 
             *                In the process of randomly selecting states in
             *                the state space to attempt to go towards, the
             *                algorithm may in fact choose the actual goal state, if
             *                it knows it, with some probability. This probability
             *                is a real number between 0.0 and 1.0; its value should
             *                usually be around 0.05 and should not be too large. It
             *                is probably a good idea to use the default value. */
            void setGoalBias(double goalBias)
            {
                goalBias_ = goalBias;
            }
            
            /** \brief Get the goal bias the planner is using */
            double getGoalBias() const
            {
                return goalBias_;
            }

            /** \brief Set a different nearest neighbors datastructure */
            template<template<typename T> class NN>
            void setNearestNeighbors()
            {
                nn_.reset(new NN<Motion*>());
            }
            
            /** \brief Option that delays collision checking procedures.
             *                When it is enabled, all neighbors are sorted by cost. The
             *                planner then goes through this list, starting with the lowest
             *                cost, checking for collisions in order to find a parent. The planner
             *                stops iterating through the list when a collision free parent is found.
             *                This prevents the planner from collision checking each neighbor, reducing
             *                computation time in scenarios where collision checking procedures are expensive.*/
            void setDelayCC(bool delayCC)
            {
                delayCC_ = delayCC;
            }
            
            /** \brief Get the state of the delayed collision checking option */
            bool getDelayCC() const
            {
                return delayCC_;
            }
            
            /** \brief Controls whether the tree is pruned during the search. This pruning removes
             *                a vertex if and only if it \e and all its descendents passes the pruning condition.
             *                The pruning condition is whether the lower-bounding estimate of a solution
             *                constrained to pass the the \e vertex is greater than the current solution.
             *                Considering the descendents of a vertex prevents removing a descendent
             *                that may actually be capable of later providing a better solution once
             *                its incoming path passes through a different vertex (e.g., a change in homotopy class). */
            void setTreePruning(const bool prune);
            
            /** \brief Get the state of the pruning option. */
            bool getTreePruning() const
            {
                return useTreePruning_;
            }

            /** \brief Use the measure of the pruned subproblem instead of the measure of the entire problem domain (if such an expression exists and a solution is present).
             *            Currently the only method to calculate this measure in closed-form is through a informed sampler, so this option also requires that. */
            void setPrunedMeasure(bool informedMeasure);

            /** \brief Use direct sampling of the heuristic for the generation of random samples (e.g., x_rand).
             *           If a direct sampling method is not defined for the objective, rejection sampling will be used by default. */
            void setInformedSampling(bool informedSampling);
            
            /** \brief Controls whether heuristic rejection is used on samples (e.g., x_rand) */
            void setSampleRejection(const bool reject);

            unsigned int numIterations() const
            {
                return iterations_;
            }
            
            ompl::base::Cost bestCost() const
            {
                return bestCost_;
            }
            
        protected:
            
            /** \brief Representation of a motion */
            class Motion
            {
            public:
                /** \brief Constructor that allocates memory for the state. This constructor automatically allocates memory for \e state, \e cost, and \e incCost */
                Motion(const base::SpaceInformationPtr &si) :
                state(si->allocState()),
                parent(NULL)
                {
                }
                
                ~Motion()
                {
                }
                
                /** \brief The index of the 2D graph associated to this state */
                int index=-1;
                
                /** \brief The state contained by the motion */
                base::State       *state;
                
                /** \brief The parent motion in the exploration tree */
                Motion            *parent;
                
                /** \brief The cost up to this motion */
                base::Cost        cost;
                
                /** \brief The incremental cost of this motion's parent to this motion (this is stored to save distance computations in the updateChildCosts() method) */
                base::Cost        incCost;
                
                /** \brief The set of motions descending from the current motion */
                std::vector<Motion*> children;
            };
            
            /** \brief Create the samplers */
            void allocSampler();
            
            /** \brief Generate a 3D sample that is directly 2D-connected to graph2DnodeIndex and makes the tree grow */
            bool sampleGrowingUniformNear2D(ompl::base::State* statePtr, int graph2DnodeIndex);
            
            /** \brief Free the memory allocated by this planner */
            void freeMemory();
            
            // For sorting a list of costs and getting only their sorted indices
            struct CostIndexCompare
            {
                CostIndexCompare(const std::vector<base::Cost>& costs,
                                 const base::OptimizationObjective &opt) :
                                 costs_(costs), opt_(opt)
                                 {}
                                 bool operator()(unsigned i, unsigned j)
                                 {
                                     return opt_.isCostBetterThan(costs_[i],costs_[j]);
                                 }
                                 const std::vector<base::Cost>& costs_;
                                 const base::OptimizationObjective &opt_;
            };
            
            /** \brief Compute distance between motions (actually distance between contained states) */
            double distanceFunction(const Motion *a, const Motion *b) const
            {
                return si_->distance(a->state, b->state);
            }
            
            /** \brief Gets the neighbours of a given motion, using either k-nearest of radius as appropriate. */
            void getNeighbors(Motion *motion, std::vector<Motion*> &nbh) const;
            
            /** \brief Removes the given motion from the parent's child list */
            void removeFromParent(Motion *m);
            
            /** \brief Updates the cost of the children of this node if the cost up to this node has changed */
            void updateChildCosts(Motion *m);
            
            /** \brief Prunes all those states which time is higher than maxtime.
             *  Returns the number of motions pruned.*/
            int pruneTree();
            
            /** \brief Add the children of a vertex to the given list. */
            void addChildrenToList(std::queue<Motion*, std::deque<Motion*> > *motionList, Motion* motion);
            
            /**
             * @brief Checks wheter the given motion has a time greater than maxTime
             * 
             * @param motion a motion to be checked
             * @param maxTime the current maximum time, all the motions above this will return false
             * @return bool
             */
            bool keepCondition(const Motion* motion, double maxTime) const;
            
            /** \brief A nearest-neighbors datastructure containing the tree of motions */
            boost::shared_ptr< NearestNeighbors<Motion*> > nn_;
            
            /** \brief The fraction of time the goal is picked as the state to expand towards (if such a state is available) */
            double                                         goalBias_;
                        
            /** \brief The random number generator */
            RNG                                            rng_;
                        
            /** \brief Option to delay and reduce collision checking within iterations */
            bool                                           delayCC_;
            
            /** \brief Objective we're optimizing */
            base::OptimizationObjectivePtr                 opt_;
            
            /** \brief The most recent goal motion.  Used for PlannerData computation */
            Motion                                         *lastGoalMotion_;
            
            /** \brief A list of states in the tree that satisfy the goal condition */
            std::vector<Motion*>                           goalMotions_;
            
            /** \brief The status of the tree pruning option. */
            bool                                           useTreePruning_;
            
            /** \brief Stores the start states as Motions. */
            std::vector<Motion*>                           startMotions_;
            
            /** \brief Best cost found so far by algorithm */
            base::Cost                                     bestCost_;
            
            /** \brief Number of iterations the algorithm performed */
            unsigned int                                   iterations_;
            
            ///////////////////////////////////////
            // Planner progress property functions
            std::string numIterationsProperty() const
            {
                return boost::lexical_cast<std::string>(numIterations());
            }
            std::string bestCostProperty() const
            {
                return boost::lexical_cast<std::string>(bestCost());
            }
            

            //Mirko from now on, functions and data structure related to the mrt algorithm
            
            /**
             * @brief given a container which supports iterators, pick a random element and return it
             * 
             * @param c the container from which to sample
             * @return a copy of a random element from the container
             */
            template <class container,class contained>
            contained sampleFromContainer(const container& c)
            {
                int i = rng_.uniformInt(0,c.size()-1);
                auto it=c.begin();
                std::advance(it,i);
                return *it;
            }
            
            /**
             * @brief Uses growing_nodes in order to generate a new sample, also updates growing_nodes
             * 
             * @param rstate The sampled state
             * @return bool
             */
            bool sampleGrowing(ompl::base::State* rstate);
            
            /**
             * @brief returns a pointer to the best goal motion
             * 
             * @return Motion*
             */
            Motion* getBestGoalMotion();
            
            /**
             * @brief Checks if the 2D node has outer arcs to be explored, if not, removes it from \ref growing_nodes
             * 
             * @param node the index of the node to check
             * @return bool
             */
            bool checkAndEraseGrowingNode(int node);
            
            /**
             * @brief Updates the informed sampling data structure \ref informedSamplingGraph which is used to improve the solution
             * 
             * @return double the maximum time allowed for any new sample to be useful for improving the solution
             */
            void updateSamplingGraph();
            
            /**
             * @brief Samples a state from the \ref informedSamplingGraph
             * 
             * @param rstate the random state sampled
             * @return bool if a sample was found
             */
            bool sampleRefine(ompl::base::State* rstate);
            
            double computeDistanceBetweenNodes(double x, double y, double x1, double y1);
            
            /**
             * @brief max_speed pixel/s of the agents, this is used to check if a motion is valid (i.e. slow enough to be executed)
             * Note that this is the speed in pixel, not in meters!
             */
            double max_speed;
            
            
            /**
             * @brief This is our voronoi 2D graph, represented as nodes and outer edges
             */
            graph_msgs::GeometryGraph graph;
            
            /**
             * @brief Fast accessor to the indexes of the nodes of the graph given x and y, use as node_index=xy_index[x][y]
             */
            std::map<int,std::map<int,int>> xy_index;
            
            
            /**
             * @brief Fast accessor to the predecessors of a node, i.e. the nodes which have an outer arc to i
             */
            std::vector<std::set<int>> predecessors;
            
            /**
             * @brief vector <list<motion*>>
             * from 2D graph node index to a list of 3D motions, in order to get the state use graph.nodes[first].x,y and second[j] as time
             */
            std::vector<std::list<Motion*>> explored_tree;
            
            /**
             * @brief from graph index to bool if a motion is already present in the \ref explored_tree
             */
            std::vector<bool> explored_nodes;
            
            /**
             * @brief list of indexes of nodes that can still be used for growing (i.e. they still have unexplored outer arcs)
             */
            std::set<int> growing_nodes;
            
            /**
             * @brief Informed sampling graph, the nodes inside are candidates for improving the solution
             */
            std::vector<int> informedSamplingGraph;
            
            
            /**
             * @brief This represents the current solution path, used for the informed sampling as a biased set of nodes
             */
            std::vector<Motion *> solution_path;

            private:
            bool debugStream;
            std::ostream* mat=NULL;
        };
    }
}

#endif
