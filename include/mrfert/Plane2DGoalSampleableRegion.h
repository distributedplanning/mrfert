#ifndef PLANE2DGOALSAMPLEABLEREGION_H
#define PLANE2DGOALSAMPLEABLEREGION_H

#include <ompl/base/goals/GoalRegion.h>
#include <ompl/base/goals/GoalSampleableRegion.h>

class Plane2DGoalSampleableRegion: public ompl::base::GoalSampleableRegion
{
public:
    Plane2DGoalSampleableRegion(const ompl::base::SpaceInformationPtr& si,int x, int y);
    virtual double distanceGoal(const ompl::base::State* state) const;
    virtual bool isSatisfied(const ompl::base::State* st, double* distance) const;
    virtual void sampleGoal(ompl::base::State* st) const;
    virtual unsigned int maxSampleCount() const;
private:
    int x;
    int y;
    mutable ompl::RNG rng_;
};


#endif // PLANE2DGOALSAMPLEABLEREGION_H
