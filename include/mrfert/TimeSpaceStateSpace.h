#ifndef OMPL_BASE_SPACES_TIME_SPACE_STATE_SPACE_
#define OMPL_BASE_SPACES_TIME_SPACE_STATE_SPACE_

#include "ompl/base/StateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"

#define XYT_STATE_SPACE_TYPE 1

#include "ompl/base/OptimizationObjective.h"
#include "ompl/base/objectives/PathLengthOptimizationObjective.h"
#include "ompl/base/StateSampler.h"
#include "graph_msgs/GeometryGraph.h"



namespace ompl
{
    namespace base
    {
        
        /** \brief A state space representing XYT */
        class XYTStateSpace : public CompoundStateSpace
        {
        public:
            
            /** \brief A state in XYT: position = (x, y), time = (t) */
            class StateType : public CompoundStateSpace::StateType
            {
            public:
                StateType() : CompoundStateSpace::StateType()
                {
                }
                
                inline double get(unsigned int index) const
                {
                    return as<RealVectorStateSpace::StateType>(index)->values[0];
                }
                
                inline double& get(unsigned int index)
                {
                    return as<RealVectorStateSpace::StateType>(index)->values[0];
                }
                
                /** \brief Get the X component of the state */
                inline double getX() const
                {
                    return as<RealVectorStateSpace::StateType>(0)->values[0];
                }
                
                /** \brief Get the Y component of the state */
                inline double getY() const
                {
                    return as<RealVectorStateSpace::StateType>(1)->values[0];
                }
                
                /** \brief Get the T component of the state */
                inline double getT() const
                {
                    return as<RealVectorStateSpace::StateType>(2)->values[0];
                }
                                
                /** \brief Set the X component of the state */
                inline void setX(double x)
                {
                    as<RealVectorStateSpace::StateType>(0)->values[0] = x;
                }
                
                /** \brief Set the Y component of the state */
                inline void setY(double y)
                {
                    as<RealVectorStateSpace::StateType>(1)->values[0] = y;
                }
                
                /** \brief Set the T component of the state */
                inline void setT(double t)
                {
                    as<RealVectorStateSpace::StateType>(2)->values[0] = t;
                }
                
                /** \brief Set the X, Y and Z components of the state */
                inline void setXYZ(double x, double y, double t)
                {
                    setX(x);
                    setY(y);
                    setT(t);
                }
                
            };
            
//             virtual void interpolate(const State *from, const State *to, const double t, State *state) const;
            
            virtual double distance(const ompl::base::State* state1, const ompl::base::State* state2) const;
            
            XYTStateSpace() : CompoundStateSpace(),bounds_(3)
            {
                setName("XYTStateSpace" + getName());
                type_ = STATE_SPACE_TYPE_COUNT+XYT_STATE_SPACE_TYPE;
                addSubspace(StateSpacePtr(new RealVectorStateSpace(1)), 1.0);
                addSubspace(StateSpacePtr(new RealVectorStateSpace(1)), 1.0);
                addSubspace(StateSpacePtr(new RealVectorStateSpace(1)), 10);
                lock();
            }
            
            virtual ~XYTStateSpace()
            {
            }
            
            /** \copydoc RealVectorStateSpace::setBounds() */
            void setBounds(const RealVectorBounds &bounds)
            {
                as<RealVectorStateSpace>(0)->setBounds(bounds.low[0],bounds.high[0]);
                as<RealVectorStateSpace>(1)->setBounds(bounds.low[1],bounds.high[1]);
                as<RealVectorStateSpace>(2)->setBounds(bounds.low[2],bounds.high[2]);
                bounds_.setLow(0,as<RealVectorStateSpace>(0)->getBounds().low[0]);
                bounds_.setLow(1,as<RealVectorStateSpace>(1)->getBounds().low[0]);
                bounds_.setLow(2,as<RealVectorStateSpace>(2)->getBounds().low[0]);
                bounds_.setHigh(0,as<RealVectorStateSpace>(0)->getBounds().high[0]);
                bounds_.setHigh(1,as<RealVectorStateSpace>(1)->getBounds().high[0]);
                bounds_.setHigh(2,as<RealVectorStateSpace>(2)->getBounds().high[0]);
            }
            
            virtual StateSamplerPtr allocStateSampler() const;
            
            /** \copydoc RealVectorStateSpace::getBounds() */
            const RealVectorBounds& getBounds() const
            {
                return bounds_;
            }
            
            virtual State* allocState() const;
            virtual void freeState(State *state) const;
            
            virtual void registerProjections();
            
        private:
            RealVectorBounds bounds_;
        };
    }
}

#endif
