#ifndef SINGLE_AGENT_PLANNER_H
#define SINGLE_AGENT_PLANNER_H
#include <geometry_msgs/Polygon.h>
#include <graph_msgs/GeometryGraph.h>
#include <ros/node_handle.h>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <mrfert/locking.h>
#include "Plane2DEnvironment.h"
#include <mutex>

class my_rviz_visual_tools:public rviz_visual_tools::RvizVisualTools
{
public:
    my_rviz_visual_tools(std::string base_frame_, std::string marker_topic_);
    bool publishPath(const std::vector< geometry_msgs::Point >& path, const rviz_visual_tools::colors& color = rviz_visual_tools::RED, const rviz_visual_tools::scales& scale = rviz_visual_tools::REGULAR, const std::string& ns = "Path", int id = 0);
};

class single_agent_planner
{
public:
    single_agent_planner(graph_msgs::GeometryGraph graph, std::string name, int priority,int start,int end);

    void setPosition(double x, double y);
    void run();
    void resetUpdate();
    bool updated_solution(std::vector< mrfert::node >& sol);
    void init();
    


private:
    ros::NodeHandle nh;
    void agent_callback(const mrfert::locking::ConstPtr& msg);
    void publishSolution(    
                             std::vector<node3D>& sol_, 
                             rviz_visual_tools::colors color_,
                             std::string name_, ros::Time zero);
    std::vector<geometry_msgs::Point> convert2DSolution(std::vector<node3D>& sol, ros::Time zero);
    
    graph_msgs::GeometryGraph graph;
    double speed;
    double max_time;
    double map_scale;
    double graph_scale;
    double x;
    double y;
    int global_x, global_y, start_x,start_y;
    std::shared_ptr<Plane2DEnvironment> env;
    my_rviz_visual_tools rviz;
    std::string agent_name;
    std::map<int, mrfert::locking> last_locking_received;
    int priority;
    mrfert::locking lck;
    std::mutex collision_mutex;
    ros::Subscriber agents;
    ros::Publisher locking_pub;
    void send_locking_info(const mrfert::locking& msg);
    void alive_callback(const mrfert::locking::ConstPtr& msg);
    ros::Publisher keep_alive;
    ros::Subscriber sub_alive;
    mrfert::locking alive;
    double dist(double x,double y, double x1, double y1);
    bool getNextNode(mrfert::node& n);
    bool collision;
    bool updated;
    ros::Time last_updated, first_planned;
};














#endif