#ifndef PLANE2DENVIRONMENT_H
#define PLANE2DENVIRONMENT_H

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/goals/GoalRegion.h>
#include <ompl/base/DiscreteMotionValidator.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/util/PPM.h>
#include <ompl/config.h>

#include <mrfert/TimeSpaceStateSpace.h>
#include <mrfert/TimeSpaceLengthOptimizationObjective.h>
#include <mrfert/Plane2DGoalRegion.h>
#include <mrfert/Plane2DMotionValidator.h>
#include <mrfert/Plane2DStateSampler.h>
#include <boost/smart_ptr/make_shared_object.hpp>
#include "mrfert/MRTstar.h"
#include <mrfert/node.h>
#include <algorithm>

namespace ob = ompl::base;
namespace og = ompl::geometric;


struct node3D_old
{
    int x,y;
    double t_start;  
    double t_end;
    operator mrfert::node()
    {
        mrfert::node n;
        n.x=x;
        n.y=y;
        n.t_start=ros::Time(t_start);
        n.t_end=ros::Time(t_end);
        return n;
    }
};

typedef mrfert::node node3D;

/**
 * @brief A simple wrapper of some OMPL setup stuff, basically loads everything, plans and shows the solution
 * 
 */
class Plane2DEnvironment
{
public:
    
    Plane2DEnvironment(const char* ppm_file, graph_msgs::GeometryGraph graph, double max_speed, double maxTime, double occupied = 5);

    Plane2DEnvironment(graph_msgs::GeometryGraph graph, double max_speed, double maxTime, double occupied = 5, double maxWidth = 3000, double maxHeight = 3000);
    
    void IntermediateSolutionCallback(const ob::Planner *, const std::vector< const ob::State * > & path, const ob::Cost cost);
    
    bool plan(unsigned int start_row, unsigned int start_col, unsigned int goal_row, unsigned int goal_col, double time);
    
    void clearOccupiedMatrix();
    
    void addOccupiedInfo(std::vector< node3D > nodes, ros::Time zero = ros::Time(), std::string name="");
    
    void recordSolution(std::ostream& os);
    
    void getSolution(std::vector< node3D >& nodes, ros::Time zero = ros::Time());
    
    void save(const char *filename);
    void setDebugStream(std::ostream* mat)
    {
        this->mat=mat;
        (pl_->as<og::MRTstar>())->setDebugStream(mat);
    }
    
private:
    
    /**
     * @brief This is the starting point for collision detection between environment and agents
     * 
     * @param state The state to check
     * @return bool
     */
    bool isStateValid(const ob::State *state) const;
    void createPlanner(double max_speed);

    /**
     * @brief Mirko how do I write this in order to be fast? I need to check x y and time... but x and y are mapped in graph
     *    If I use the xy_index map, I will have to do m*log(n)^2 comparisons, where m is the number of occupied nodes, and n the total number of nodes
     *    Another option would be to increase the state space in order to add a fake discrete state which is the index
     *    But then I would pay a lot for state instantiation... but only m comparisons
     *    Or I could trust the state to be a node, and use a O(1) array to access the state... 
     */
    std::vector<std::vector<std::vector<double>>> occupied_matrix;
    
    
    og::SimpleSetupPtr ss_;
    ob::PlannerPtr pl_;
    int maxWidth_;
    int maxHeight_;
    int maxTime_;
    double occupied_;
    ompl::PPM ppm_;
    graph_msgs::GeometryGraph graph;
    std::ostream* mat=NULL;
    ros::Time last_called_solve;
    
};

#endif // PLANE2DENVIRONMENT_H
