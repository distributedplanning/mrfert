#ifndef PLANE2DGOALREGION_H
#define PLANE2DGOALREGION_H
#include <ompl/base/goals/GoalRegion.h>


class Plane2DGoalRegion: public ompl::base::GoalRegion
{
public:
    Plane2DGoalRegion(const ompl::base::SpaceInformationPtr& si,int x, int y);
    virtual double distanceGoal(const ompl::base::State* state) const;
    virtual bool isSatisfied(const ompl::base::State* st, double* distance) const;
private:
    int x;
    int y;
};

#endif // PLANE2DGOALREGION_H
