#ifndef PLANE2DSTATESAMPLER_H
#define PLANE2DSTATESAMPLER_H
#include <ompl/base/StateSampler.h>
#include <ompl/base/StateSpace.h>
#include <graph_msgs/GeometryGraph.h>

namespace ompl
{
    namespace base
    {
        class Plane2DStateSampler: public ompl::base::StateSampler
        {
        public:
            virtual void sampleGaussian(State* state, const State* mean, const double stdDev);
            virtual void sampleUniform(State* state);
            virtual void sampleUniformNear(State* state, const State* near, const double distance);
            virtual bool sample(State* state);
            virtual bool sampleNear(State* state, const State* near, const double distance);
            Plane2DStateSampler(const StateSpace* space,const graph_msgs::GeometryGraph& graph);
            virtual ~Plane2DStateSampler(){
                        std::cout<<"sampled "<<sampled<<std::endl;
            };
            
        private:
            graph_msgs::GeometryGraph graph;
            ompl::RNG rng_;
            std::map<int,std::map<int,int>> xy_index;
            
            std::vector<bool> explored_nodes; //These nodes will not grow during the growing part            
            
            
            static int sampled;
        };
    }
}

#endif // PLANE2DSTATESAMPLER_H
