#ifndef PLANE2DMOTIONVALIDATOR_H
#define PLANE2DMOTIONVALIDATOR_H

#include <ompl/base/MotionValidator.h>
#include <ompl/base/DiscreteMotionValidator.h>
#include <map>

#include "mrfert/TimeSpaceStateSpace.h"
// 
namespace ob = ompl::base;
typedef ob::XYTStateSpace::StateType XYTState;


class Plane2DMotionValidator: public ob::DiscreteMotionValidator
{
public:
    
    /**
     * @brief Constructor, takes the graph (just in case) and max_speed
     * 
     * @param si SpaceInformation, from OMPL
     * @param graph 2D graph, not currently used
     * @param max_speed max speed of the agent in pixel/s, not in meters!
     */
    Plane2DMotionValidator(const ompl::base::SpaceInformationPtr& si, const graph_msgs::GeometryGraph& graph, double max_speed);
    
    virtual bool checkMotion(const ompl::base::State* s1, const ompl::base::State* s2, std::pair< ompl::base::State*, double >& lastValid) const;

    virtual bool checkMotion(const ompl::base::State* s1, const ompl::base::State* s2) const;
    
    virtual ~Plane2DMotionValidator(){
//         std::cout<<"checked "<<checked<<" motion checks"<<std::endl;
        
//         std::cout<<"skipped "<<skipped<<" motion checks"<<std::endl;

//         std::cout<<"dropped "<<too_fast<<" motion checks"<<std::endl;
    }
private:
    double max_speed;
    const graph_msgs::GeometryGraph& graph;
    static int skipped, too_fast, checked;
    std::map<int,std::map<int,int>> xy_index;
};

#endif // PLANE2DMOTIONVALIDATOR_H
