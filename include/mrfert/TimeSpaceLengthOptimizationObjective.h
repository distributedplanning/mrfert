#ifndef TIMESPACELENGTHOPTIMIZATIONOBJECTIVE_H
#define TIMESPACELENGTHOPTIMIZATIONOBJECTIVE_H
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>

namespace ompl
{
    namespace base
    {
        /** \brief An optimization objective which corresponds to optimizing path length. */
        class TimeSpaceLengthOptimizationObjective : public PathLengthOptimizationObjective
        {
        public:
            TimeSpaceLengthOptimizationObjective(const ompl::base::SpaceInformationPtr& si);
            
            /** \brief Returns identity cost. */
            virtual Cost stateCost(const State *s) const;
            
            /** \brief Motion cost for this objective is defined as
             *               the configuration space distance between \e s1 and \e
             *               s2, using the method SpaceInformation::distance(). */
            virtual Cost motionCost(const State *s1, const State *s2) const;
            virtual bool isSatisfied(Cost c) const;
        };
    }
}

#endif // TIMESPACELENGTHOPTIMIZATIONOBJECTIVE_H
